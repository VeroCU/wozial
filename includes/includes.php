<?php
$whatsIconClass="";
$hoy=date("Y-m-j");
$hora=date('H:i', strtotime("+1 hours"));
/* %%%%%%%%%%%%%%%%%%%% MENSAJES               */
	if($mensaje!=''){
		$mensajes='
			<div class="uk-container">
				<div uk-grid>
					<div class="uk-width-1-1 margen-v-20">
						<div class="uk-alert-'.$mensajeClase.'" uk-alert>
							<a class="uk-alert-close" uk-close style="color:#000!important"></a>
							'.$mensaje.'
						</div>					
					</div>
				</div>
			</div>';
	}
	$consultaServicios = $CONEXION -> query("SELECT * FROM productos WHERE estatus = 1");

	$menuMovil="";

/* %%%%%%%%%%%%%%%%%%%% RUTAS AMIGABLES        */
		$rutaInicio			=	'Inicio';
		$rutaPedido			=	$ruta.'Revisar_orden';

/* %%%%%%%%%%%%%%%%%%%% MENU                   */
	$menu='
		<li class="'.$nav1.'"><a href="'.$rutaInicio.'">Inicio</a></li>
		';

	while ($servicios = $consultaServicios -> fetch_assoc() ) {
		
		$menuMovil.='
			<li class="uk-width-1-1 uk-grid-collapse container-btn">
				<a class="btn btn-1 menu-txt btn4 '.$nav1.'" href="'.$servicios['id'].'_servicios.php"> <span class="color-negro titles-borde">
				_'.$servicios['titulo'].' '.$servicios['titulo_dorado'].'</span> 
				</a>
			</li>

		';
	}
	/*debug($menuMovil);
	breackpoint($menuMovil);*/

/* %%%%%%%%%%%%%%%%%%%% HEADER                 */
	$header='
		<!---MODAL COTIZACION--->
		<div id="modal-center" class="uk-flex-top" uk-modal>
			<div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical" style="background:#000;border-radius:16px;">
				<div class="margin-top-30" style="padding:8px 10px">
			        <button class="uk-modal-close-default" type="button" uk-close style="width:40px"></button>
			      
			        <div class="margin-top-50">
			        	<input type="text" name="nombre" placeholder="NOMBRE" id="m_nombre" required>
			        	<div id="nombre_m" class="nombre_m"> ANOTA TU NOMBRE </div>
			        </div>
			        <div class="margin-top-10">
			        	<input type="email" name="mail" placeholder="CORREO" id="m_email" required>
			        	<div id="mail_m" class="nombre_m"> ANOTA TU E-MAIL CORRECTO</div>
			        </div>
			        <div class="margin-top-10">
			        	<input type="number" name="what" placeholder="WHATSAPP" id="m_what" minlength="10" maxlength="10" required>
			        	<div id="what_m" class="nombre_m"> WHATSAPP 10 DIJITOS VALIDOS</div>
			        </div>
			        <div class="margin-top-10">
			        	<input type="" name="" placeholder="¿CÓMO PODEMOS APOYARTE?" id="m_txt">
			        </div>
			        <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid borders-0" style="margin-bottom:0;padding-bottom:0">
			            <label><input class="uk-checkbox" type="checkbox" id="mensaje_llamada"> </label> 
			            <span style="color:#fff; font-size:14px;"> AGENDAR LLAMADA</span>
			        </div>
			        <div class="" uk-grid style="margin-top:0;padding-top:0">
			        	<div class="uk-width-1-1@s uk-width-1-2@m" style="margin-top:0;padding-top:0">
				        	<input type="date" id="start" name="trip-start" value="'.$hoy.'">
					    </div>
					    <div class="uk-width-1-1@s uk-width-1-4@m" style="margin-top:0;padding-top:0">
				        	<input type="time" name="usr_time" id="time" value="'.$hora.'">
					    </div>
			        </div>
			        <div class="uk-flex uk-flex-center uk-align-center">
				        <div id="btn_cotizacion" class="uk-disabled" uk-grid style="width:140px">
							<a class="uk-button uk-button-default k-align-center uk-button-primary margin-left-10" href="#modal-center" id="cotizasend" uk-toggle>
							    <p style="padding:10px;padding-top:7px;">Enviar</p>
							</a>
						</div>
					</div>
			    </div>
			</div>
		</div>
		<div class="uk-width-1-6 uk-grid-collapse nav-hamburguesa">
			<a href="#menu-movil" uk-toggle class="uk-icon-button"><i class="fa fa-bars fa-1x" aria-hidden="true" style="font-size:25px"></i></a>
		</div>
		<!---FIN MODAL COTIZACION--->
		<div class="uk-offcanvas-content uk-position-relative">
			<header class="uk-width-3-4 uk-align-left menu-pc" style="position:absolute;">
				<div class="uk-width-1-1 uk-grid-collapse">
					<div uk-grid class="left-0 uk-grid-collapse">

						<!-- Menú escritorio -->
						<div class="uk-width-1-4@m uk-visible@s uk-grid-collapse" >
							<nav class="uk-navbar-container uk-navbar-transparent uk-grid-collapse" uk-navbar>
								<div class="uk-navbar-center uk-grid-collapse margin-top-20">
									<a href="Inicio">
										<img src="./img/design/logo.png">
									</a>
								</div>
							</nav>
						</div>
						<div class="uk-width-expand@m uk-text-center uk-grid-collapse left-0" uk-grid>
						    <div class="uk-width-expand@m uk-grid-collapse uk-text-right header-text margin-right-40header">
						    	OFICINAS:  '.$telefono.' / 
						    	<a href="'.$socialWhats.'" target="_blank">WHATSAPP: '.$telefono1.'</a>
						    </div>
						    <!--div class="uk-width-auto@m uk-grid-collapse uk-text-right margin-right-30header header-text">ES / EN &nbsp;&nbsp;
						    </div-->
						</div>
						<!-- Botones de Logeo
						<div class="uk-width-1-1">
							'.$loginButton.'
						</div> -->
					</div>
				</div>
			</header>
			<header class="uk-width-1-1 uk-align-left menu-movil" style="position:absolute;z-index:9;">
				<div uk-grid class="left-0 uk-grid-collapse margin-top-15" >
						<div class="uk-width-1-3 uk-grid-collapse">
							<nav class="uk-navbar-container uk-navbar-transparent uk-grid-collapse" uk-navbar>
								<div class="uk-navbar-center uk-grid-collapse">
									<a href="Inicio">
										<img class="movilLogo"  src="./img/design/logo.png">
									</a>
								</div>
							</nav>
						</div>
						<div class="uk-width-expand uk-text-center left-0 uk-align-right margin-right-30header" uk-grid style="">
						    <div class="uk-width-auto uk-grid-collapse uk-align-middle uk-text-right bordes-0 ">
						        	<a href="<?= $socialWhats ?>" target="_blank" class="uk-icon-link menu-txt" ztyle="color:#000;font-size:14px;" uk-icon="whatsapp"></a> &nbsp;&nbsp;
						        	<a href="<?= $socialInst ?>" target="_blank" class="uk-icon-link menu-txt" ztyle="color:#000;font-size:14px;" uk-icon="instagram"></a> &nbsp;&nbsp;
						        	<a href="<?= $socialFace ?>" target="_blank" class="uk-icon-link menu-txt" ztyle="color:#000;font-size:14px;" uk-icon="facebook"></a> <br>
						        	<!--strong class="uk-text-right" style="font-size:16px;color:#000"> ES / EN </strong-->
						    </div>
						</div>
					</div>
			</header>
			'.$mensajes.'
			<style type="text/css">
			.background-menu {
				height:100vh;width:100%;
				margin:0;
				padding:0;
			  	-webkit-box-sizing: border-box;
			    box-sizing: border-box;
			background-image: -webkit-gradient(linear, right bottom, left top, color-stop(49.9%, #000), color-stop(50.1%, #fff));
			background-image: -webkit-linear-gradient(bottom right, #000 49.9%, #fff 50.1%);
			background-image: -o-linear-gradient(bottom right, #000 49.9%, #fff 50.1%);
			background-image: linear-gradient(to top left, #000 49.9%, #fff 50.1%);
			}
			</style>
			
			<!-- Menú móviles -->
			<div id="menu-movil" uk-offcanvas="mode: none;overlay: true" class="transicion animated fadeIn">
				<div class="uk-offcanvas-bar uk-flex uk-flex-column"  id="close-efecto" style="margin:0;padding:0;z-index:9999999;height:100vh;width:100%;">
					<section class="full-container uk-grid-collapse menu-pc">
						<!---->
						<div class="background-menu">
							
							<div class="uk-width-auto uk-align-right uk-grid-collapse uk-align-left" style="float:left;margin:0;padding:40px;padding-top:0;padding-bottom:0">
								<nav class="uk-navbar-container uk-navbar-transparent uk-grid-collapse" uk-navbar>
									<div class="uk-navbar-center uk-grid-collapse top-logo">
										<a href="Inicio"> <img src="./img/design/logo.png" style="padding-bottom:0; margin:40px 0 0 40px;"> </a>
									</div>
								</nav>
							</div>
							<div class="uk-width-auto padding-h-20 uk-grid-collapse uk-text-right uk-align-right close-menus"  style="margin:0;padding:0;">
								<button class="uk-offcanvas-close close btn-close-menu" type="button" uk-close  id="close-menu"
									style="color:#ba9d69!important;margin-right: 18px!important;width:50px;margin-right:136px!important"></button>
							</div>
							<div class="uk-width-1-1" uk-grid>
								<div class="uk-width-1-1@s uk-width-expand@m margin-left-100">
									<div class="uk-width-1-1@s left-0 uk-align-center margin-top-20 ">
										<div class="uk-grid-collapse uk-text-left left-0" style="color:#000;margin:0;padding:0;">
											<ul class="uk-width-1-1 menu-list" uk-grid style="margin:0;padding:0;">
												'.$menuMovil.'
												<li class="uk-width-1-1 uk-grid-collapse container-btn">
													<a class="btn btn-1 menu-txt btn4 '.$nav1.'" href="portafolio" style=""> <span class="color-negro titles-borde">_PORTAFOLIO</span> </a>
												</li>
											</ul>
										</div>
									</div>
									<div class="uk-margin-top color-negro" style="">
										<hr class="footer-Hr">
										<a class="uk-text-left uk-grid-collapse uk-link-reset texto-borde" href="empezamos"	style="text-align:left!important;float:left;line-height:0.6em;">_ ¿COMO EMPEZAMOS?</a> <br>
										<a class="uk-text-left uk-grid-collapse uk-link-reset texto-borde" href="partners" style="text-align:left!important;float:left;line-height:0.6em;">_ WOZIAL PARTNERS</a> <br>
										<a class="uk-text-left uk-grid-collapse uk-link-reset texto-borde" href="1_politicas" style="text-align:left!important;float:left;line-height:0.6em;">_ AVISO DE PRIVACIDAD</a> <br>
									</div>
								</div>
								<div class="uk-width-1-1@s uk-width-1-3@m uk-align-right">
									<div class="uk-text-left" style="position:absolute;bottom:10%">
										<hr class="footer-Hr-direccion">
										<a class="uk-grid-collapse cero-bordes" href="'. $socialWhats.'" target="_blank"> 
											<span class="uk-grid-collapse cero-bordes" 
											uk-icon="icon: whatsapp; ratio: 1.2"></span> </a>
										<a class="uk-grid-collapse cero-bordes" href="'. $socialFace .'" target="_blank"> 
											<span class="uk-grid-collapse cero-bordes" 
											uk-icon="icon: facebook; ratio: 1.2"></span> </a>
										<a class="uk-grid-collapse cero-bordes" href="'.$socialInst .'" target="_blank"> 
											<span class="uk-grid-collapse cero-bordes" 
											uk-icon="icon: instagram; ratio: 1.2"></span> </a>
										<a class="uk-grid-collapse cero-bordes" href="https://goo.gl/maps/ae9F5ZWJVXums8zMA" target="_blank"> 
											<span class="uk-grid-collapse cero-bordes" 
											uk-icon="icon: location; ratio: 1.2"></span> </a>
										<p class="uk-grid-collapse uk-text-left text-8 color-blanco" 
											style="font-weight: 100; margin:0; padding:0">
											TEL. 3323016610<br> 
											WHATSAPP: 3338096501<br>
											AVENIDA LAPIZLÁZULI<br> 2074 INT. 3<br>
											RESIDENCIAL VICTORIA
										</p>
										<hr class="footer-Hr-direccion">
									</div>
								</div>
							</div>

						</div>
					</section>
					
					<section class="uk-grid-collapse menu-movil">
							
							<div class="uk-navbar-center uk-grid-collapse top-logo container-logo">
								<a href="Inicio"> <img src="./img/design/logo-wozial.png" style="width:50%;margin-top: -30px;"> </a>
							</div>
							<div class="uk-width-auto padding-h-20 uk-grid-collapse uk-text-right uk-align-right close-menus"  style="margin:0;padding:0;">
								<button class="uk-offcanvas-close close btn-close-menu" type="button" uk-close  id="close-menu"
									style="color:#ba9d69!important;margin-right: 18px!important;width:40px"></button>
							</div>
							<div class="uk-width-1-1" uk-grid style="margin-left:0!important">
								<div class="uk-width-1-1@s">
									<div class="uk-width-1-1@s left-0 uk-align-center margin-top-20 ">
										<div class="uk-grid-collapse uk-text-left left-0" style="color:#000;margin:0;padding:0;">
											<ul class="uk-width-1-1 menu-list left-0" uk-grid style="margin:0;padding:0;">
												'.$menuMovil.'
												<li class="uk-width-1-1 uk-grid-collapse container-btn">
													<a class="btn btn-1 menu-txt btn4 '.$nav1.'" href="portafolio" style=""> <span class="titles-borde">_PORTAFOLIO</span> </a>
												</li>
											</ul>
										</div>
									</div>
									<div class="uk-margin-top color-negro" style="">
										<hr class="footer-Hr">
										<a class="uk-text-left uk-grid-collapse uk-link-reset texto-borde" href="empezamos"	style="text-align:left!important;float:left;line-height:0.6em;">_ ¿COMO EMPEZAMOS?</a> <br>
										<a class="uk-text-left uk-grid-collapse uk-link-reset texto-borde" href="partners" style="text-align:left!important;float:left;line-height:0.6em;">_ WOZIAL PARTNERS</a> <br>
										<a class="uk-text-left uk-grid-collapse uk-link-reset texto-borde" href="1_politicas" style="text-align:left!important;float:left;line-height:0.6em;">_ AVISO DE PRIVACIDAD</a> <br>
									</div>
								</div>
								<div class="uk-width-1-1@s padding-v-20-50" style="background:#000;">
									<div class="uk-text-left" style="">
										<hr class="footer-Hr-direccion">
										<a class="uk-grid-collapse cero-bordes" href="'. $socialWhats.'" target="_blank"> 
											<span class="uk-grid-collapse cero-bordes" 
											uk-icon="icon: whatsapp; ratio: 1.2"></span> </a>
										<a class="uk-grid-collapse cero-bordes" href="'. $socialFace .'" target="_blank"> 
											<span class="uk-grid-collapse cero-bordes" 
											uk-icon="icon: facebook; ratio: 1.2"></span> </a>
										<a class="uk-grid-collapse cero-bordes" href="'.$socialInst .'" target="_blank"> 
											<span class="uk-grid-collapse cero-bordes" 
											uk-icon="icon: instagram; ratio: 1.2"></span> </a>
										<a class="uk-grid-collapse cero-bordes" href="https://goo.gl/maps/ae9F5ZWJVXums8zMA" target="_blank"> 
											<span class="uk-grid-collapse cero-bordes" 
											uk-icon="icon: location; ratio: 1.2"></span> </a>
										<p class="uk-grid-collapse uk-text-left text-8 color-blanco" 
											style="font-weight: 100; margin:0; padding:0">
											TEL. 3323016610<br> 
											WHATSAPP: 3338096501<br>
											AVENIDA LAPIZLÁZULI<br> 2074 INT. 3<br>
											RESIDENCIAL VICTORIA
										</p>
										<hr class="footer-Hr-direccion">
									</div>
								</div>
							</div>
						
					</section>

				</div>
			</div>';

/* %%%%%%%%%%%%%%%%%%%% FOOTER                 */
	$stickerClass=($carroTotalProds==0 OR $identificador==500 OR $identificador==501 OR $identificador==502)?'uk-hidden':'';
	$footer = '
		<!--
		<footer>
			<div class="bg-footer" style="z-index: 0;">
				<div class="uk-container uk-position-relative">
					<div class="uk-width-1-1 uk-text-center">
						<div class="padding-v-50">
							'.date('Y').' todos los derechos reservados Diseño por <a href="https://wozial.com/" target="_blank" class="color-negro">Wozial Marketing Lovers</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		-->
		<div id="cotizacion-fixed" class="uk-position-top uk-height-viewport '.$stickerClass.'">
			<div>
				<a href="'.$rutaPedido.'"><img src="img/design/checkout.png"></a>
			</div>
		</div>

		'.$loginModal.'

		<!-- <div id="totop" style="border:solid green">
			<a href="#top" uk-scroll id="arrow-button" class="uk-icon-button uk-button-totop uk-box-shadow-large oscuro"><i class="fa fa-arrow-up fa-1x" aria-hidden="true"></i></a>
		</div> -->

		<!-- Whatsapp Plugin -->
		<!--<div id="whatsapp-plugin" class="uk-hidden">
				<div id="whats-head" class="uk-position-relative">
					<div uk-grid class="uk-grid-small uk-grid-match">
						<div>
							<div class="uk-flex uk-flex-center uk-flex-middle">
								<img class="uk-border-circle padding-10" src="img/design/logo-whatsapp.jpg" style="width:70px;">
							</div>
						</div>
						<div>
							<div class="uk-flex uk-flex-center uk-flex-middle color-blanco">
								<div>
									<span class="text-sm">Salutem</span><br>
									<span class="text-6 uk-text-light">Atención en línea vía chat</span>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-position-right color-blanco text-sm">
						<span class="pointer padding-10" id="whats-close">x</spam>
					</div>
				</div>
				<div id="whats-body-1" class="uk-flex uk-flex-middle">
					<div class="bg-white uk-border-rounded padding-h-10" style="margin-left:20px;">
						<img src="library/whats/loading.gif" style="height:40px;">
					</div>
				</div>
				<div id="whats-body-2" class="uk-hidden">
					<span class="uk-text-bold uk-text-muted">'.$Brand.'</span><br>
					Hola 👋<br>
					¿Cómo puedo ayudarte?
				</div>
				<div id="whats-footer" class="uk-flex uk-flex-center uk-flex-middle">
					<a href="'.$socialWhats.'" target="_blank" class="uk-button uk-button-small" id="button-whats"><i class="fab fa-whatsapp fa-lg"></i> <span style="font-weight:400;">Comenzar chat</span></a>
				</div>
			</div>
			<div id="whats-show" class="'.$whatsIconClass.' pointer uk-border-circle color-white uk-box-shadow-large" style="background-color: rgb(9, 94, 84);">
				<i class="fab fa-3x fa-whatsapp"></i>
			</div> -->
		<!-- Whatsapp Plugin -->
	
	</div>';

/* %%%%%%%%%%%%%%%%%%%% HEAD GENERAL                */
	$headGNRL='
		<html lang="'.$languaje.'">
		<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">

			<meta charset="utf-8">
			<title>'.$title.'</title>
			<meta name="description" content="'.$description.'" />
			<meta property="fb:app_id" content="'.$appID.'" />
			<link rel="image_src" href="'.$ruta.$logoOg.'" />

			<meta property="og:type" content="website" />
			<meta property="og:title" content="'.$title.'" />
			<meta property="og:description" content="'.$description.'" />
			<meta property="og:url" content="'.$rutaEstaPagina.'" />
			<meta property="og:image" content="'.$ruta.$logoOg.'" />

			<meta itemprop="name" content="'.$title.'" />
			<meta itemprop="description" content="'.$description.'" />
			<meta itemprop="url" content="'.$rutaEstaPagina.'" />
			<meta itemprop="thumbnailUrl" content="'.$ruta.$logoOg.'" />
			<meta itemprop="image" content="'.$ruta.$logoOg.'" />

			<meta name="twitter:title" content="'.$title.'" />
			<meta name="twitter:description" content="'.$description.'" />
			<meta name="twitter:url" content="'.$rutaEstaPagina.'" />
			<meta name="twitter:image" content="'.$ruta.$logoOg.'" />
			<meta name="twitter:card" content="summary" />

			<meta name="viewport"       content="width=device-width, initial-scale=1">

			<link rel="icon"            href="'.$ruta.'img/design/favicon.ico" type="image/x-icon">
			<link rel="shortcut icon"   href="img/design/favicon.ico" type="image/x-icon">
			<link rel="stylesheet"      href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
			<link rel="stylesheet"      href="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/css/uikit.min.css" />
			<link rel="stylesheet"      href="css/general.css">
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Lexend+Deca&display=swap">
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Questrial&display=swap">
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap">
			<link rel="stylesheet"      href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
			<link rel="stylesheet"      href="https://cdn.rawgit.com/filipelinhares/ress/master/dist/ress.min.css" type="text/css" >
			<link rel="stylesheet"      href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">


			<!-- jQuery is required -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

			<!-- UIkit JS -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/js/uikit.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/js/uikit-icons.min.js"></script>
		</head>';


/* %%%%%%%%%%%%%%%%%%%% SCRIPTS                */
	$scriptGNRL='
	
		<script src="js/slick-animation.min.js"></script>
		<script src="js/general.js"></script>
		
		<script>
			$("input").attr("autocomplete","off");
		</script>

		<script>
			$(".cantidad").keyup(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					$(this).val(inventario);
				}
				console.log(inventario+" - "+cantidad);
			})
			$(".cantidad").focusout(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					//console.log(inventario*2+" - "+cantidad);
					$(this).val(inventario);
				}
			})

			// Agregar al carro
			$(".buybutton").click(function(){
				var id=$(this).data("id");
				var cantidad=$("#"+id).val();

				$.ajax({
					method: "POST",
					url: "addtocart",
					data: { 
						id: id,
						cantidad: cantidad,
						addtocart: 1
					}
				})
				.done(function( msg ) {
					datos = JSON.parse(msg);
					UIkit.notification.closeAll();
					UIkit.notification(datos.msg);
					$("#cartcount").html(datos.count);
					$("#cotizacion-fixed").removeClass("uk-hidden");
				});
			})
		</script>

		<script type="text/javascript">
			$("#close-menu").click(function() {
				var element = document.getElementById("close-efecto");
				element.classList.add("animated pulse");
			});
		</script>
		';



	// Script login Facebook
	$scriptGNRL.=(!isset($_SESSION['uid']) AND $dominio != 'localhost' AND isset($facebookLogin))?'
		<script>
			// Esta es la llamada a facebook FB.getLoginStatus()
			function statusChangeCallback(response) {
				if (response.status === "connected") {
					procesarLogin();
				} else {
					console.log("No se pudo identificar");
				}
			}

			// Verificar el estatus del login
			function checkLoginState() {
				FB.getLoginStatus(function(response) {
					statusChangeCallback(response);
				});
			}

			// Definir características de nuestra app
			window.fbAsyncInit = function() {
				FB.init({
					appId      : "'.$appID.'",
					xfbml      : true,
					version    : "v3.2"
				});
				FB.AppEvents.logPageView();
			};

			// Ejecutar el script
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/es_LA/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, \'script\', \'facebook-jssdk\'));
			
			// Procesar Login
			function procesarLogin() {
				FB.api(\'/me?fields=id,name,email\', function(response) {
					console.log(response);
					$.ajax({
						method: "POST",
						url: "includes/acciones.php",
						data: { 
							facebooklogin: 1,
							nombre: response.name,
							email: response.email,
							id: response.id
						}
					})
					.done(function( response ) {
						console.log( response );
						datos = JSON.parse( response );
						UIkit.notification.closeAll();
						UIkit.notification(datos.msj);
						if(datos.estatus==0){
							location.reload();
						}
					});
				});
			}
		</script>

		':'';


// Reportar actividad
	$scriptGNRL.=(!isset($_SESSION['uid']))?'':'
		<script>
			var w;
			function startWorker() {
			  if(typeof(Worker) !== "undefined") {
			    if(typeof(w) == "undefined") {
			      w = new Worker("js/activityClientFront.js");
			    }
			    w.onmessage = function(event) {
					//console.log(event.data);
			    };
			  } else {
			    document.getElementById("result").innerHTML = "Por favor, utiliza un navegador moderno";
			  }
			}
			startWorker();
		</script>
		';

		

/* %%%%%%%%%%%%%%%%%%%% BUSQUEDA               */
	$scriptGNRL.='
		<script>
			$(document).ready(function(){
				$(".search").keyup(function(e){
					if(e.which==13){
						var consulta=$(this).val();
						var l = consulta.length;
						if(l>2){
							window.location = ("'.$ruta.'"+consulta+"_gdl");
						}else{
							UIkit.notification.closeAll();
							UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
						}
					}
				});
				$(".search-button").click(function(){
					var consulta=$(".search-bar-input").val();
					var l = consulta.length;
					if(l>2){
						window.location = ("'.$ruta.'"+consulta+"_gdl");
					}else{
						UIkit.notification.closeAll();
						UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
					}
				});
			});

			$(document).ready(function(){
				$("#start").hide();
				    $("#time").hide();
					$("#mensaje_llamada").click(function(){
				    if($("#mensaje_llamada").prop("checked")){
				    	$("#start").show();
				    	$("#time").show();
				    }else{
				    	$("#start").hide();
				    	$("#time").hide();
				    }
				});

			});
			


			// script para modal de mensajes
			$(document).ready(function() {
			    $("#cotizasend").click(function(){
				    var nombre = $("#m_nombre").val();;
				    var whatsapp = $("#m_what").val();
				    var email = $("#m_email").val();
				    var txt =  $("#m_txt").val();
				    var fallo=0;
				    if($("#mensaje_llamada").prop("checked")){
				      	var llamada = 1;
				     }else{
				      	var llada =0;
				    }
			      
				    var dia =  $("#start").val();
				    var hora =  $("#time").val();
  					var alerta = "";
			      
			    
			      $("input").removeClass("uk-form-danger");
			    
			      var parametros = {
			        "mensajes" : 1,
			        "nombre" : nombre,
			        "email" : email,
			        "whatsapp" : whatsapp,
			        "txt" : txt
			      };

			      if(llamada) parametros.llamada =llamada;
			      if(dia) parametros.dia = dia;
			      if(hora) parametros.hora = hora;

				console.log("params",parametros);
				 if (fallo == 0) {
			        $.ajax({
			          data:  parametros,
			          url:   "includes/acciones.php",
			          type:  "post",
			          beforeSend: function () {
			            $("#cotizasend").html("<div uk-spinner></div>");
			            $("#cotizasend").prop("disabled",true);
			            $("#cotizasend").disabled = true;
			            UIkit.notification.closeAll();
			            UIkit.notification(\'<section style="padding:0;margin:0!important;height:100vh;width:100vw"> <div style="background-color:rgba(255,255,255, 0.8); width:100vw; height:100vh;border:solid 0.003em "> <div style="color: #bf9d59;font-size:20px; text-align: center;font-weight:400;margin-top:18%;">Estamos enviando tu mensaje</div><div style="margin:40%;"> <img style="height:10%;width:30%" src="http://creativdeziners.co.uk/wp-content/uploads/2015/03/emailMarketing.gif" style=""> </div>  </div> </section> \');
			          },
			          success:  function (msj) {

			            $("#cotizasend").html("Enviar");
			            $("#cotizasend").disabled = false;
			            $("#cotizasend").prop("disabled",false);
			            console.log(msj);
			            datos = JSON.parse(msj);
			            UIkit.notification.closeAll();
			            UIkit.notification(datos.msj);
			            if (datos.estatus==0) {
			              $("#mensaje_txt").val("");
			              $("#mensaje_email").val("");
			              $("#mensaje_whatssapp").val("");
			              $("#mensaje_email").val("");
			            }
			          }
			        })
			      }else{
			        UIkit.notification.closeAll();
			        UIkit.notification(\' <div class="uk-text-center color-blanco bg-danger padding-10 text-lg"><i uk-icon="icon:warning;ratio:2;"></i> &nbsp; \' +alerta+\' </div>\' );
			        $("#"+id).focus();
			        $("#"+id).addClass("uk-form-danger");
			      }

			  	});
			 });
					
		</script>';


	$scriptGNRL.='
		<script>
			$("#whats-close").click(function(){
				$("#whatsapp-plugin").addClass("uk-hidden");
				$("#whats-show").removeClass("uk-hidden");
				$.ajax({
					method: "POST",
					url: "includes/acciones.php",
					data: { 
						whatsappHiden: 1
					}
				})
				.done(function( msg ) {
					console.log(msg);
				});
			});
			$("#whats-show").click(function(){
				$("#whatsapp-plugin").removeClass("uk-hidden");
				$("#whats-show").addClass("uk-hidden");
				$("#whats-body-1").addClass("uk-hidden");
				$("#whats-body-2").removeClass("uk-hidden");
				$.ajax({
					method: "POST",
					url: "includes/acciones.php",
					data: { 
						whatsappShow: 1
					}
				})
				.done(function( msg ) {
					console.log(msg);
				});
			});
		</script>';

	$scriptGNRL.='
		<script>
			var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
			var nombre="";
			var mail=""; 
			var what="";
			var validMail = false;

			$("#m_nombre").keyup(function(){
			  nombre = $(this).val();
				if(nombre.length <= 0	){
					$("#nombre_m").removeClass("nombre_m");
					$("#nombre_m").addClass("nombre_ver");
				}
				else{
					$("#nombre_m").addClass("nombre_m");
					$("#nombre_m").removeClass("nombre_ver");
				}

				if ((nombre != "") && validMail && (what != "")) { 
					$("#btn_cotizacion").removeClass("uk-disabled");
				}
				else{
					$("#btn_cotizacion").addClass("uk-disabled");
				}
			});

			$("#m_email").keyup(function(){
				mail = $(this).val();
				if(mail.length <= 0	){
					$("#mail_m").removeClass("nombre_m");
					$("#mail_m").addClass("nombre_ver");
				}
				else{
					if (!regex.test(mail)) {
						$("#mail_m").removeClass("nombre_m");
						$("#mail_m").addClass("nombre_ver");
						validMail=false;
					} else {
						validMail=true;
						$("#mail_m").addClass("nombre_m");
						$("#mail_m").removeClass("nombre_ver");
					}
				}
				if ((nombre != "") && validMail && (what != "")) { 
					$("#btn_cotizacion").removeClass("uk-disabled");
				}
				else{
					$("#btn_cotizacion").addClass("uk-disabled");
				}
			});

			$("#m_what").keyup(function(){
				what = $(this).val();
				if(what.length <= 0	){
					console.log("cc", what.length);
					$("#what_m").removeClass("nombre_m");
					$("#what_m").addClass("nombre_ver");
				}
				else{
					if(what.length >= 11){						
						$("#what_m").removeClass("nombre_m");
						$("#what_m").addClass("nombre_ver");
					}
					else{
						$("#what_m").addClass("nombre_m");
						$("#what_m").removeClass("nombre_ver");
					}
				}


				if ((nombre != "") && validMail && (what != "")) { 
					$("#btn_cotizacion").removeClass("uk-disabled");
				}
				else{
					$("#btn_cotizacion").addClass("uk-disabled");
				}
			});
			

		</script>
	';



