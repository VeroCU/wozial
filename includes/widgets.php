<?php
// CARRO DE COMPRA       
  //unset($_SESSION['carro']);
  if (isset($_POST['emptycart'])) {
    unset($_SESSION['carro']);
  }

  $carroTotalProds=0;
  // Si ya hay productos en la variable de sesión
  if(isset($_SESSION['carro'])){
    $arreglo=$_SESSION['carro'];
    foreach ($arreglo as $key => $value) {
      $carroTotalProds+=$value['Cantidad'];
    }
  }

  // Remover artículos del carro
  if (isset($_POST['removefromcart'])) {
    $id=$_POST['id'];
    $arregloAux=$_SESSION['carro'];
    unset($arreglo);
    $num=0;
    foreach ($arregloAux as $key => $value) {
      if ($id!=$value['Id']) {
        $arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$arregloAux[$num]['Cantidad']);
      }
      $num++;
    }
    $_SESSION['carro']=$arreglo;
  }

  // Agregar artículos al carro
  if (isset($_POST['addtocart'])) {
    if (isset($_POST['cantidad']) and $_POST['cantidad']!==0 and $_POST['cantidad']!=='') {
      $id=$_POST['id'];

      $carroTotalProds+=$_POST['cantidad'];
      $arregloNuevo[]=array('Id'=>$id,'Cantidad'=>$_POST['cantidad']);

      if (!isset($arreglo)) {
        $arreglo=$arregloNuevo;
      }else{
        $arregloAux=$arreglo;
        unset($arreglo);
        $num=0;
        foreach ($arregloAux as $key => $value) {
          if ($id!=$arregloAux[$num]['Id']) {
            $arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$arregloAux[$num]['Cantidad']);
          }else{
            $carroTotalProds-=$arregloAux[$num]['Cantidad'];
          }
          $num++;
        }
        if ($_POST['cantidad']>0) {
          $arreglo[]=array('Id'=>$id,'Cantidad'=>$_POST['cantidad']);
        }
      }
      
      echo '{ "msg":"<div class=\'uk-text-center color-blanco bg-success padding-10 text-lg\'><i class=\'fa fa-check\'></i> &nbsp; Agregado al pedido</div>", "count":'.$carroTotalProds.' }';

      $_SESSION['carro']=$arreglo;
    }
  }

  if (isset($_POST['actualizarcarro'])) {
    $arregloAux=$_SESSION['carro'];
    unset($arreglo);
    $carroTotalProds=0;
    $num=0;
    foreach ($arregloAux as $key => $value) {
      $arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$_POST['cantidad'.$num]);
      $carroTotalProds+=$_POST['cantidad'.$num];
      $num++;
    }
    $_SESSION['carro']=$arreglo;
  }

// LIMITAR PALABRAS      
  function wordlimit($string, $length , $ellipsis)
  {
    $words = explode(' ', strip_tags($string));
    if (count($words) > $length)
    {
      return implode(' ', array_slice($words, 0, $length)) ." ". $ellipsis;
    }
    else
    {
      return $string;
    }
  }

// FECHA                 
  // FECHA CORTA
    function fechaCorta($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      return $fechaD.'-'.$fechaM.'-'.$fechaY;
    }
    
  // FECHA Y HORA
    function fechaHora($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaH=date('H',$fechaSegundos);
      $fechaI=date('i',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      return $fechaD.'-'.$fechaM.'-'.$fechaY.'<br>'.$fechaH.':'.$fechaI;
    }
    
  // SOLO HORA
    function soloHora($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaH=date('H',$fechaSegundos);
      $fechaI=date('i',$fechaSegundos);

      return $fechaH.':'.$fechaI;
    }

  function fechaSQL($fechaSQL){
    $fechaSegundos=strtotime($fechaSQL);

    $fechaY=date('Y',$fechaSegundos);
    $fechaM=date('m',$fechaSegundos);
    $fechaD=date('d',$fechaSegundos);
   
    return $fechaY.'/'.$fechaM.'/'.$fechaD;
  }
  
  // FECHA DIA
    function fechaDisplayDia($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      switch ($fechaDay) {
        case 'mon':
        $fechaDia='Lunes';
        break;
        case 'tue':
        $fechaDia='Martes';
        break;
        case 'wed':
        $fechaDia='Miércoles';
        break;
        case 'thu':
        $fechaDia='Jueves';
        break;
        case 'fri':
        $fechaDia='Viernes';
        break;
        case 'sat':
        $fechaDia='Sábado';
        break;
        default:
        $fechaDia='Domingo';
        break;
      }
      return $fechaDia;
    }

  // FECHA MES
    function fechaDisplayMes($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      switch ($fechaM) {
        case 1:
        $mes='enero';
        break;
        
        case 2:
        $mes='febrero';
        break;
        
        case 3:
        $mes='marzo';
        break;
        
        case 4:
        $mes='abril';
        break;
        
        case 5:
        $mes='mayo';
        break;
        
        case 6:
        $mes='junio';
        break;
        
        case 7:
        $mes='julio';
        break;
        
        case 8:
        $mes='agosto';
        break;
        
        case 9:
        $mes='septiembre';
        break;
        
        case 10:
        $mes='octubre';
        break;
        
        case 11:
        $mes='noviembre';
        break;
        
        default:
        $mes='diciembre';
        break;
      }

      return $mes;
    }

  // FECHA LARGA
    function fechaDisplay($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      switch ($fechaM) {
        case 1:
        $mes='enero';
        break;
        
        case 2:
        $mes='febrero';
        break;
        
        case 3:
        $mes='marzo';
        break;
        
        case 4:
        $mes='abril';
        break;
        
        case 5:
        $mes='mayo';
        break;
        
        case 6:
        $mes='junio';
        break;
        
        case 7:
        $mes='julio';
        break;
        
        case 8:
        $mes='agosto';
        break;
        
        case 9:
        $mes='septiembre';
        break;
        
        case 10:
        $mes='octubre';
        break;
        
        case 11:
        $mes='noviembre';
        break;
        
        default:
        $mes='diciembre';
        break;
      }

      switch ($fechaDay) {
        case 'mon':
        $fechaDia='Lunes';
        break;
        case 'tue':
        $fechaDia='Martes';
        break;
        case 'wed':
        $fechaDia='Miércoles';
        break;
        case 'thu':
        $fechaDia='Jueves';
        break;
        case 'fri':
        $fechaDia='Viernes';
        break;
        case 'sat':
        $fechaDia='Sábado';
        break;
        default:
        $fechaDia='Domingo';
        break;
      }

      return $fechaDia.' '.$fechaD.' de '.$mes.' de '.$fechaY;
    }

// CARRUSEL              
  // Carousel Inicio
    function carousel($carousel){
      global $CONEXION;
      global $dominio;

      $CONSULTA= $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");
      $row_CONSULTA = $CONSULTA -> fetch_assoc();
      switch ($row_CONSULTA['slideranim']) {
        case 0:
          $animation='fade';
          break;
        case 1:
          $animation='slide';
          break;
        case 2:
          $animation='scale';
          break;
        case 3:
          $animation='pull';
          break;
        case 4:
          $animation='push';
          break;
        default:
          $animation='fade';
          break;
      }
      $CAROUSEL = $CONEXION -> query("SELECT * FROM $carousel ORDER BY orden");
      $numPics=$CAROUSEL->num_rows;
      if ($numPics>0) {
        echo '
            <!-- Start Carousel -->
            <div uk-slideshow="autoplay:true;ratio:'.$row_CONSULTA['sliderproporcion'].';animation:'.$animation.';min-height:'.$row_CONSULTA['sliderhmin'].';max-height:'.$row_CONSULTA['sliderhmax'].';" class="uk-grid-collapse" uk-grid>
              <div class="uk-visible-toggle uk-width-1-1 uk-flex-first">
                <div class="uk-position-relative">
                  <ul class="uk-slideshow-items">';
                    $num=0;
                    $activo=' active';
                    while ($row_CAROUSEL = $CAROUSEL -> fetch_assoc()) {
                      $caption='';
                      if (strlen($row_CAROUSEL['url'])>0) {
                        $pos=strpos($row_CAROUSEL['url'], $dominio);
                        $target=($pos>0)?'':'target="_blank"';
                        if ($row_CONSULTA['slidertextos']==1 AND strlen($row_CAROUSEL['titulo'])>0 AND strlen($row_CAROUSEL['url'])>0) {
                          $caption='
                          <div class="uk-position-bottom uk-transition-slide-bottom">
                            <div style="min-width:200px;min-height:100px;" class="uk-text-center">
                              <a href="'.$row_CAROUSEL['url'].'" '.$target.' class="uk-button uk-button-white uk-button-large">
                                '.$row_CAROUSEL['titulo'].'
                              </a>
                            </div>
                          </div>';
                        }
                      }
                      echo '
                          <li>
                            <img src="img/contenido/'.$carousel.'/'.$row_CAROUSEL['id'].'.jpg" uk-cover>
                            '.$caption.'
                          </li>';
                    }

                    echo '
                  </ul>

                  <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                  <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

                </div>
                <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
              </div>
            </div>
            <!-- End Carousel -->
            ';
      }
      mysqli_free_result($CAROUSEL);
    }




// ITEM                   
  function item($id){
    global $CONEXION;
    global $caracteres_si_validos;
    global $caracteres_no_validos;

    $widget    = '';
    $style     = 'max-width:200px;';  
    $noPic     = 'img/design/camara.jpg';
    $rutaPics  = 'img/contenido/productos/';
    $firstPic  = $noPic;

    $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $id");
    $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
    $link=$id.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($row_CONSULTA1['titulo'])))).'-.html';

    // Fotografía
      $CONSULTA3 = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $id ORDER BY orden,id LIMIT 1");
      while ($rowCONSULTA3 = $CONSULTA3 -> fetch_assoc()) {
        $firstPic = $rutaPics.$rowCONSULTA3['id'].'.jpg';
      }

      $picWidth=0;
      $picHeight=0;
      $picSize=getimagesize($firstPic);
      foreach ($picSize as $key => $value) {
        if ($key==3) {
          $arrayCadena1=explode(' ',$value);
          $arrayCadena1=str_replace('"', '', $arrayCadena1);
          foreach ($arrayCadena1 as $key1 => $value1) {

            $arrayCadena2=explode('=',$value1);
            foreach ($arrayCadena2 as $key2 => $value2) {
              if (is_numeric($value2)) {
                $picProp[]=$value2;
              }
            }
          }
        }
      }
      if (isset($picProp)) {
        $picWidth=$picProp[0];
        $picHeight=$picProp[1];

        $style=($picWidth<$picHeight)?'max-height:200px;':$style;
      }

    $widget.='
      <div id="item'.$id.'" class="uk-text-center">
        <div class="bg-white padding-20" style="border:solid 1px #CCC;">
          <a href="'.$link.'" style="color:black;">
            <div class="margin-10">
              <div class="uk-flex uk-flex-center uk-flex-middle" style="height: 200px;">
                <img data-src="'.$firstPic.'" uk-img style="'.$style.'">
              </div>
              <div style="min-height:100px;">
                <div>
                  '.$row_CONSULTA1['sku'].'
                </div>
                <div class="uk-flex uk-flex-center">
                  <div class="line-yellow"></div>
                </div>
                <div class="padding-v-10">
                </div>
                <div>
                  '.$row_CONSULTA1['titulo'].'
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>';

    return $widget;
  }

//barra de menu
$headerizq="";
$headerizq.='
        <div class="uk-width-auto@m uk-grid-collapse teleft-0 uk-visible@m">
            <div class="uk-flex uk-flex-middle height-100" >
                <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
                    <div>
                        <div class="uk-grid-collapse uk-align-center">
                            <a href="#menu-movil" uk-toggle id="menu" class="border-cero">
                                <div class="texto-vertical-2 text_menu"> 
                                  <p style="padding:0;margin:0;">&nbsp;&nbsp;MENÚ&nbsp;&nbsp;</p></div>
                            </a>
                        </div>
                    </div>
                    <div>
                        <div class="uk-grid-collapse  uk-align-center">
                            <a href="'.$socialWhats.'" target="_blank" class="uk-icon-link menu-txt" uk-icon="whatsapp" 
                            style="color:#000;padding:8px 0!important"></a><br>
                            <a href="'.$socialInst.'" target="_blank" class="uk-icon-link menu-txt" uk-icon="instagram" 
                            style="color:#000;padding:8px 0!important"></a><br>
                            <a href="'.$socialFace.'" target="_blank" class="uk-icon-link menu-txt" uk-icon="facebook" 
                            style="color:#000;padding:8px 0!important"></a><br>

                        </div>
                    </div>
                </div>
                
            </div>
        </div>
';
// slider derecha
$urlPortafolios="./img/contenido/portafolio/";
$consultaPortafolios = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1");
$sliderderecha ='
    <div class="uk-width-1-4@m uk-grid-collapse left-0 height-100 uk-visible@m">
        <div class="">
            <div class="slick-carousel height-100">';
 while ($portafolio = $consultaPortafolios -> fetch_assoc()){
    $portafolioUrl=$portafolio['url'];
    $fondo = $urlPortafolios.$portafolio['id'].'.png';
    
    $sliderderecha.='
                <div class="uk-width-auto@m" 
                style="height: calc(100vh / 3); width:calc(100vh / 3)!important">
                    <div class="uk-flex uk-flex-center uk-inline-clip uk-transition-toggle" tabindex="0" style="height:100%!important;width:100%">';
    if(strlen($portafolioUrl) > 0)
    {
          $sliderderecha.='<a class="uk-grid-collapse" href="'.$portafolioUrl.'" target="_blank">';
    }   
            $sliderderecha.='<div class="uk-background-contain uk-background-muted uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" 
                              style="height: calc(100vh / 3);width:calc(100vh / 3); background: url('.$fondo.');background-position: bottom;background-repeat:no-repeat;background-size:100% 100%;">';
                                
    if(strlen($portafolioUrl) > 0)
    {
         $sliderderecha.='
                                <div class="uk-transition-fade uk-position-cover uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle">
                                    <div class="uk-h4 uk-margin-remove uk-align-center">
                                      <img src="./img/design/logo-wozial.png" style="width:66px;margin-left:24px">
                                      <p style="color:#fff; font-weight:400">Ver Proyecto</p>
                                    </div>
                                </div>';
    }
        $sliderderecha.='   </div>';

    if(strlen($portafolioUrl) > 0)
    {
         $sliderderecha.='</a>';
    }
      $sliderderecha.='</div>
                </div>';
}
$sliderderecha .=' 
        </div>
    </div>
</div>';

//slider derecho MOVIL
$consultamovil1 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1");
$slidermovil='
        <div class="uk-width-1-1 uk-hidden@m" style="margin:40px 0 60px">
            <div uk-slider="autoplay: true">
                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                    <ul class="uk-slider-items uk-child-width-1-1@s uk-child-width-1-3@m" >
';
while ($portafolioMovil1 = $consultamovil1 -> fetch_assoc()){
    $portafolioUrl=$portafolioMovil1['url'];
    $fondo = $urlPortafolios.$portafolioMovil1['id'].'.png';

    $slidermovil.='
                        <li class="uk-flex uk-flex-center uk-flex-middle">';
    if(strlen($portafolioUrl) > 0)
    {
          $slidermovil.='<a class="uk-grid-collapse" href="'.$portafolioUrl.'" target="_blank">';
    }   
            $slidermovil.='<div class="uk-background-contain uk-background-muted uk-height-medium uk-panel" 
                              style="height: calc(100vh / 2);width:calc(100vh / 2); background: url('.$fondo.');background-position: bottom;background-repeat:no-repeat;background-size:100% 100%;">
                            </div>';

    if(strlen($portafolioUrl) > 0)
    {
         $slidermovil.='</a>';
    }
      $slidermovil.='</li>
    ';
    }
$slidermovil.='
                    </ul>
                </div>
                <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
            </div>
        </div>
';

//boton portafolio derecha
$btnportafolio="";
$btnportafolio.='
    <div class="uk-width-auto@m uk-grid-collapse teleft-0 height-100 uk-visible@m"  
    style="margin-top:0!important;">
        <div class="uk-grid-collapse uk-align-center"  style="margin-top:0!important;">
            <a href="portafolio" uk-toggle id="menu" class="border-cero">
                <div class="texto-vertical-2" style="margin-top:30px;margin-right:20px"> PORTAFOLIO &nbsp;</div>
            </a>
        </div>
    </div>
';

           



