<?php
if (isset($_GET['cat'])) {/*
	$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catNAME=$row_CATEGORY['txt'];
	$catParentID=$row_CATEGORY['parent'];

	$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catParent=$row_CATEGORY['txt'];
*/
	echo '
	<div class="uk-width-1-1 margen-v-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?seccion='.$seccion.'">Productos</a></li>
			<!--
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias&cat='.$catParentID.'">'.$catParent.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'">'.$catNAME.'</a></li>
			-->
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo&cat='.$cat.'" class="color-red">Nuevo</a></li>
		</ul>
	</div>';
}else{
	echo '
	<div class="uk-width-1-1 margen-v-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo" class="color-red">Nuevo</a></li>
		</ul>
	</div>';
}
?>

<form action="index.php" class="uk-width-1-1" method="post" name="editar" onsubmit="return checkForm(this);">
	<input type="hidden" name="nuevo" value="1">
	<input type="hidden" name="seccion" value="<?=$seccion?>">

	<div uk-grid class="uk-grid-small uk-child-width-1-3@l uk-child-width-1-2@m">
		<div  class="uk-width-1-4">
			<label class="uk-text-uppercase" for="sku">sku</label>
			<input type="text" class="uk-input" name="sku" placeholder="sku">
		</div>
		<div class="uk-width-1-4">
			<label class="uk-text-capitalize" for="titulo">Titulo</label>
			<input type="text" class="uk-input" name="titulo" required>
		</div>
		<div class="uk-width-1-4">
			<label class="uk-text-capitalize" for="titulo_dorado">Titulo Dorado</label>
			<input type="text" class="uk-input" name="titulo_dorado" required>
		</div>
		<div class="uk-width-1-4">
			<label class="uk-text-capitalize" for="subtitulo">Subtitulo</label>
			<input type="text" class="uk-input" name="subtitulo">
		</div>
		<div class="uk-width-1-4">
			<label class="uk-text-capitalize" for="subtitulo_dorado">Subtitulo Dorado</label>
			<input type="text" class="uk-input" name="subtitulo_dorado">
		</div>
		<div class="uk-width-1-4">
			<label class="uk-text-capitalize" for="precio">Precio</label>
			<input type="texto" class="uk-input" name="precio" placeholder="Desde $2,800 mensuales" min="0">
		</div>
		<div class="uk-width-1-1">
			<div class="margen-top-20">
				<label for="txt">Descripción</label>
				<textarea class="editor" name="txt"></textarea>
			</div>
		</div>

		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="title">titulo google</label>
			<input type="text" class="uk-input" name="title" placeholder="Término como alguien nos buscaría">
		</div>
		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="metadescription">descripción google</label>
			<textarea class="uk-textarea" name="metadescription" placeholder="Descripción explícita para que google muestre a quienes nos vean en las búsquedas"></textarea>
		</div>
		<div class="uk-width-1-1 uk-text-center">
			<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
			<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
		</div>
	</div>
</form>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<?php $scripts='
$(function(){
	$("#datepicker").datepicker();
	$( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
});
'; ?>