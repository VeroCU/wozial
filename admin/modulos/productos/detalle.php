<?php
$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$row_catalogo = $consulta -> fetch_assoc();
$id=$row_catalogo['id'];


$fechaSQL=$row_catalogo['fecha'];
$segundos=strtotime($fechaSQL);
$fechaUI=date('m/d/Y',$segundos);


echo '
	<div uk-grid class="uk-width-1-1 margin-v-20">
		<div class="uk-width-auto">	
			<ul class="uk-breadcrumb uk-text-capitalize">
				<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
				<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$row_catalogo['titulo'].' '.$row_catalogo['titulo_dorado'].'</a></li>
			</ul>
		</div>
		<div class="uk-width-expand uk-text-right" id="buttons">
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="uk-icon-button-2 uk-button-primary uk-box-shadow-large" uk-icon="icon:pencil;ratio:1.4;"> Editar &nbsp;&nbsp;</a> &nbsp;&nbsp;
			
			<button data-id="'.$row_catalogo['id'].'" class="eliminaprod uk-icon-button-2 uk-button-danger uk-box-shadow-large" uk-icon="icon:trash;ratio:1.4;">Eliminar &nbsp;&nbsp;</button> &nbsp;&nbsp;

			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo&cat='.$cat.'" class="uk-icon-button-2 uk-button-primary uk-box-shadow-large" style="background: #5a5;" uk-icon="icon:plus;ratio:1.4;"> Nuevo &nbsp;&nbsp;</a> &nbsp;&nbsp;
		</div>
	</div>

	<div class="uk-width-1-1 margin-v-20">

		<div class="uk-card uk-card-default uk-card-body">
			<div uk-grid>
				<div class="uk-width-1-2@s">
					<h3>Vista Previa</h3>
					<div>
						<span class="uk-text-capitalize uk-text-muted">Titulo:</span> <br>
						<input type="text" class="editarajax focusout uk-input" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$id.'" name="titulo" value="'.$row_catalogo['titulo'].'">

						<span class="uk-text-capitalize uk-text-muted">Titulo Dorado:</span> <br>
						<input type="text" class="editarajax focusout uk-input" data-tabla="'.$seccion.'" data-campo="titulo_dorado" data-id="'.$id.'"  name="titulo_dorado" value="'.$row_catalogo['titulo_dorado'].'">
					</div>
					<div>
						<span class="uk-text-capitalize uk-text-muted">Subtitulo:</span> <br>
						<input type="text" class="editarajax focusout uk-input" data-tabla="'.$seccion.'" data-campo="subtitulo" data-id="'.$id.'" name="subtitulo" value="'.$row_catalogo['subtitulo'].'">

						<span class="uk-text-capitalize uk-text-muted">Subtitulo Dorado:</span> <br>
						<input type="text" class="editarajax focusout uk-input" data-tabla="'.$seccion.'" data-campo="subtitulo_dorado" data-id="'.$id.'"  name="subtitulo_dorado" value="'.$row_catalogo['subtitulo_dorado'].'">
					</div>
					<div>
						<span class="uk-text-muted">Precio:</span> <br>
						<input type="text" class="editarajaxfocusout uk-input" data-tabla="'.$seccion.'" data-campo="precio" data-id="'.$id.'" name="precio" value="'.$row_catalogo['precio'].'">
					</div>
					<div>
						<span class="uk-text-capitalize uk-text-muted">Descripción :</span> <br>
						<textarea class="editarajax focusout uk-textarea" style="height: 130px;" data-tabla="'.$seccion.'" data-campo="txt" data-id="'.$id.'">'.$row_catalogo['txt'].'</textarea>
					</div>
				</div>
				<div class="uk-width-1-2@s">
					<div>
						<h3>SEO</h3>
						<span class="uk-text-capitalize uk-text-muted">Titulo google:</span>
						<input type="text" class="editarajaxfocusout uk-input" data-tabla="'.$seccion.'" data-campo="title" data-id="'.$id.'" name="title" value="'.$row_catalogo['title'].'">
					</div>
					<div class="uk-width-1-1">
						<span class="uk-text-capitalize uk-text-muted">Descripción google:</span>
						<textarea class="editarajaxfocusout uk-textarea" style="height: 130px;" data-tabla="'.$seccion.'" data-campo="metadescription" data-id="'.$id.'">'.$row_catalogo['metadescription'].'</textarea>
					</div>
					<!--IMAGEN PRINCIPAL-->
					<div class="uk-width-1-1 uk-text-center">
						<div class="uk-width-1-1 margin-top-50">
							<h3 class="uk-text-center">Imagen principal</h3>
						</div>
						<div class="uk-width-1-1">
							<div class="margin-bottom-20 uk-text-muted">
								Archivo PNG 
								600 px de ancho 
								600 px de alto
							</div>
							<div id="fileuploadermain">
								Cargar
							</div>
						</div>
						<div class="uk-width-1-1 uk-text-center margin-v-20">';

							$pic='../img/contenido/'.$seccion.'main/'.$row_catalogo['imagen'];
							if(strlen($row_catalogo['imagen'])>0 AND file_exists($pic)){
								echo '
								<div class="uk-panel uk-text-center">
									<a href="'.$pic.'" target="_blank">
										<img src="'.$pic.'" class=" uk-border-rounded margin-top-20">
									</a><br><br>
									<button class="uk-button uk-button-danger borrarpic"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
								</div>';
							}
						echo '
						</div>
					</div>
				</div>

				';



	echo '
				<div class="uk-width-1-1 uk-text-right">
					<span class="uk-text-muted">Fecha de captura:</span>
					'.$fechaUI.'
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 margin-v-20">
			<div class="uk-width-1-1 uk-margin-top">
				<div class="uk-card uk-card-default uk-card-body">';
if ($id < 4) {
	echo '
					<div class="padding-top-20 uk-text-center">
						<h3>ICONOS</h3>
					</div>
					<div uk-grid class="uk-child-width-1-3">
						<div>
							<span class="uk-text-capitalize uk-text-muted">Icono 1:</span> <br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="icono1" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['icono1'].'</textarea>
						</div>
						<div>
							<span class="uk-text-capitalize uk-text-muted">Icono 2:</span><br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="icono2" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['icono2'].'</textarea>
						</div>
						<div>
							<span class="uk-text-capitalize uk-text-muted">Icono 3:</span><br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="icono3" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['icono3'].'</textarea>
						</div>
					</div>
					<div class="uk-width-1-1 uk-margin-top uk-text-center">
						<h3>PASOS</h3>
					</div>
					<div uk-grid class="uk-child-width-1-3">
						<div>
							<span class="uk-text-capitalize uk-text-muted">Paso 1:</span> <br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="paso1" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['paso1'].'</textarea>
						</div>
						<div>
							<span class="uk-text-capitalize uk-text-muted">Paso 2:</span><br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="paso2" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['paso2'].'</textarea>
						</div>
						<div>
							<span class="uk-text-capitalize uk-text-muted">Paso 3:</span><br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="paso3" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['paso3'].'</textarea>
						</div>
						<div>
							<span class="uk-text-capitalize uk-text-muted">Paso 4:</span> <br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="paso4" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['paso4'].'</textarea>
						</div>
						<div>
							<span class="uk-text-capitalize uk-text-muted">Paso 5:</span><br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="paso5" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['paso5'].'</textarea>
						</div>
						<div>
							<span class="uk-text-capitalize uk-text-muted">Paso 6:</span><br>
							<textarea class="editarajaxfocusout uk-textarea" data-tabla="'.$seccion.'" data-campo="paso6" data-id="'.$id.'" style="height: 130px;">'.$row_catalogo['paso6'].'</textarea>
						</div>
					</div>';
}
echo '

			</div>
		</div>';


// Buttons
	echo '
	<div style="min-height:100px;">
	</div>
	';

// Configuracion 
	echo '
	<div id="config" class="modal" uk-modal>
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-outside" type="button" uk-close></button>
			<div class="uk-modal-header">
				Configurar imagen
			</div>
			<form action="index.php" method="post">
				<input type="hidden" name="editar" value="1">
				<input type="hidden" name="seccion" value="<?=$seccion?>">
				<input type="hidden" name="id" id="cfgid" value="">

				<div class="uk-margin">
					<label for="url">Link</label>
					<input type="text" id="url" name="url" class="uk-input">
				</div>
				<div class="uk-margin uk-text-center">
					<a href="" class="uk-button uk-button-default uk-modal-close uk-button-large" tabindex="10">Cancelar</a>
					<button class="uk-button uk-button-primary uk-button-large">Guardar</button>
				</div>
			</form>
		</div>
	</div>';

$scripts='


	// Eliminar foto
	function borrarfoto (id) { 
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true) { 
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarfoto: 1,
					id: id
				}
			})
			.done(function( msg ) {
				UIkit.notification.closeAll();
				UIkit.notification(msg);
				$("#"+id).addClass( "uk-invisible" );
			});
		}
	}

	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"modulos/'.$seccion.'/acciones.php?id='.$id.'",
			multiple: true,
			maxFileCount:1000,
			fileName:"uploadedfile",
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6000000,
			showFileCounter: false,
			showDelete: "false",
			showPreview:false,
			showQueueDiv:true,
			returnType:"json",
			onSuccess:function(files,data,xhr){
				console.log(data);
				var l = data[0].indexOf(".");
				var id = data[0].substring(0,l);
				$("#pics").append("';
				$scripts.='<div class=\'uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom\' id=\'"+id+"\'>';
				$scripts.='<div class=\'uk-card uk-card-default uk-card-body uk-text-center\'>';
				$scripts.='<div>';
				$scripts.='<a href=\'javascript:borrarfoto(\""+id+"\")\' class=\'uk-icon-button uk-button-danger\' uk-icon=\'trash\'></a>';
				$scripts.='</div>';
				$scripts.='<div class=\'uk-margin\' uk-lightbox>';
				$scripts.='<a href=\''.$rutaFinal.'"+data+"\'>';
				$scripts.='<img src=\''.$rutaFinal.'"+data+"\'>';
				$scripts.='</a>';
				$scripts.='</div>';
				$scripts.='</div>';
				$scripts.='</div>';
				$scripts.='");';
				$scripts.='
			}
		});
	});



	$(document).ready(function() {
		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "png",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=main&imagen=\'+data);
			}
		});

		$("#fileuploaderpdf").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "pdf",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=pdf&imagen=\'+data);
			}
		});
	});

	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&borrarPod&cat='.$cat.'&id="+id);
		} 
	});

	// Borrar foto redes
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpicredes");
		} 
	});


	';
