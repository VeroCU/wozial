<?php 
if ($cat!=false && $cat !='') {
	$CATEGORIAS = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
	$row_CATEGORIAS = $CATEGORIAS -> fetch_assoc();
	$catNAME=$row_CATEGORIAS['txt'];
	$catParentID=$row_CATEGORIAS['parent'];

	if ($catParentID!=0) {
		$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
		$row_CATEGORY = $CATEGORY -> fetch_assoc();
		$catParent=$row_CATEGORY['txt'];
	}
}

echo '
<div class="uk-width-1-2@s margen-top-20 uk-text-left">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias&cat='.$catParentID.'">'.$catParent.'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'" class="color-red">'.$catNAME.'</a></li>
	</ul>
</div>
';
?>

<div class="uk-width-medium-1-1 margen-v-50">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th style="width:20px;"  ></th>
				<th style="width:120px;" onclick="sortTable(1)"   class="pointer uk-text-left"> &nbsp;&nbsp; SKU</th>
				<th style="width:auto;"  onclick="sortTable(2)"   class="pointer uk-text-left"> &nbsp;&nbsp; titulo</th>
				<th style="width:150px;" onclick="sortTable(3)"   class="pointer uk-text-center">Material</th>
				<th style="width:90px;"  onclick="sortTable(4)"   class="pointer uk-text-center">Precio</th>
				<th style="width:70px;"  onclick="sortTable(5)"   class="pointer uk-text-center">Descuento</th>
				<th style="width:90px;"  onclick="sortTable(6)"   class="pointer uk-text-center">Activo</th>
				<th style="width:90px;"  ></th>
			</tr>
		</thead>
		<tbody class="sortable" data-tabla="<?=$seccion?>">
		<?php
		$consulta1 = $CONEXION -> query("SELECT * FROM $seccion WHERE categoria = $cat ORDER BY orden");
		while ($row_Consulta1 = $consulta1 -> fetch_assoc()) {
			$prodID=$row_Consulta1['id'];

			$picTxt='';
			$pic='../img/contenido/'.$seccion.'main/'.$row_Consulta1['imagen'];
			if(strlen($row_Consulta1['imagen'])>0 AND file_exists($pic)){
				$picTxt='
					<div class="uk-inline">
						<div class="uk-cover-container uk-border-circle" style="width:50px;height:50px;">
							<img src="'.$pic.'" uk-cover>
						</div>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
			}elseif(strlen($row_Consulta1['imagen'])>0 AND strpos($row_Consulta1['imagen'], 'ttp')>0){
				$pic=$row_Consulta1['imagen'];
				$picTxt= '
					<div class="uk-inline">
						<div class="uk-cover-container uk-border-circle" style="width:50px;height:50px;">
							<img src="'.$pic.'" uk-cover>
						</div>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
			}

			$link='index.php?rand='.rand(1,9999).'&seccion='.$seccion.'&subseccion=detalle&id='.$row_Consulta1['id'];

			$estatusIcon=($row_Consulta1['estatus']==0)?'off uk-text-muted':'on uk-text-primary';

			echo '
			<tr id="'.$prodID.'">
				<td>
					'.$picTxt.'
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">SKU: </span>
					<input value="'.$row_Consulta1['sku'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="sku" data-id="'.$prodID.'" tabindex="10">
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">titulo: </span>
					<input value="'.$row_Consulta1['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" tabindex="9">
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">Material: </span>
					<input value="'.$row_Consulta1['material'].'" class="editarajax uk-input uk-form-blank uk-text-center" data-tabla="'.$seccion.'" data-campo="material" data-id="'.$prodID.'" tabindex="8">
				</td>
				<td>
					<span class="uk-hidden">'.(10000+(1*($row_Consulta1['precio']))).'</span>
					<span class="uk-text-muted uk-hidden@m">Precio: </span>
					<input class="editarajax uk-input uk-form-blank uk-text-right@m" data-tabla="'.$seccion.'" data-campo="precio" data-id="'.$prodID.'" value="'.$row_Consulta1['precio'].'" tabindex="7">
				</td>
				<td>
					<span class="uk-hidden">'.(10000+(1*($row_Consulta1['descuento']))).'</span>
					<span class="uk-text-muted uk-hidden@m">Descuento: </span>
					<input class="editarajax descuento uk-input uk-form-blank uk-text-right@m" data-tabla="'.$seccion.'" data-campo="descuento" data-id="'.$prodID.'" value="'.$row_Consulta1['descuento'].'" tabindex="6">
				</td>
				<td class="uk-text-center@m">
					<span class="uk-text-muted uk-hidden@m">Activo: </span>
					<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="'.$seccion.'" data-campo="estatus" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['estatus'].'"></i>
				</td>
				<td class="uk-text-right@m">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
					<span data-id="'.$row_Consulta1['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
				</td>
			</tr>';
		}
		?>

		</tbody>
	</table>
</div>


<!-- Íconos de categoría -->
<div class="uk-width-1-2@s">
	<div id="fileuploader">
		Cargar
	</div>
	<div>
	<?php
	$pic='../img/contenido/'.$seccion.'cat/'.$row_CATEGORIAS['imagen'];
	if($row_CATEGORIAS['imagen']!='' AND file_exists($pic)){
		echo '
		<div class="uk-panel uk-text-center">
			<a href="'.$pic.'" target="_blank">
				<img src="'.$pic.'" class=" uk-border-rounded margen-top-20">
				<p> Tamaño recomendado <br>300x300 pixeles</p>
			</a>
		</div>';
	}else{
		echo '
		<div class="uk-panel uk-text-center">
			<p class="uk-scrollable-box"><i uk-icon="icon:image"></i><br><br>
				Falta logo de categoría<br>
				tamaño recomendado 300x300 pixeles<br>
			</p>
		</div>';
	}
	?>
	</div>
</div>



<div style="min-height:300px;">
</div>



<div>
	<div id="buttons">
		<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>&subseccion=nuevo&cat=<?=$cat?>" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<?php
$scripts='
	$(document).ready(function() {

		$(".eliminaprod").click(function() {
			var id = $(this).attr(\'data-id\');
			//console.log(id);
			var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
			if (statusConfirm == true) { 
				window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&borrarPod&cat='.$cat.'&id="+id);
			} 
		});

		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpg, jpeg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&position=categoria&imagen=\'+data);
			}
		});

		$("#fileuploaderhover").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpg, jpeg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&position=categoria2&imagen=\'+data);
			}
		});
		
	});


	';



?>