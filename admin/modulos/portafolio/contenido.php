<?php 
echo '
<div class="uk-width-1-1">
	<div class="uk-width-1-1">
		<ul class="uk-breadcrumb margin-v-20">
			<li><a href="index.php?seccion='.$seccion.'" class="color-red">portafolio</a></li>
		</ul>
	</div>
</div>


<div class="uk-width-1-1">
	<div id="fileuploader">
		Cargar
	</div>
</div>
<div class="uk-width-1-1 uk-text-center">
	<div uk-grid class="uk-grid-small uk-grid-match sortable" data-tabla="portafolio">';

$consulta1 = $CONEXION -> query("SELECT * FROM portafolio ORDER BY orden,id");
while ($row_Consulta1 = $consulta1 -> fetch_assoc()) {

	$prodID=$row_Consulta1['id'];
	$estatusIcon=($row_Consulta1['estatus']==1)?'on uk-text-primary ':'off uk-text-muted';
	$estatusIcon1=($row_Consulta1['slider1']==1)?'on uk-text-primary ':'off uk-text-muted';
	$estatusIcon2=($row_Consulta1['slider2']==1)?'on uk-text-primary ':'off uk-text-muted';
	$estatusIcon3=($row_Consulta1['slider3']==1)?'on uk-text-primary ':'off uk-text-muted';

	$pic='../img/contenido/portafolio/'.$prodID.'.png';
	if(file_exists($pic)){
		echo '
		<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$prodID.'">
			<div class=" uk-card uk-card-body uk-text-center" style="background: #ddd;">
				<div class="uk-padding-small" uk-grid>
				<div class="uk-width-1-1 uk-padding-small" style="margin-top:0;padding-top:0">Visible en:</div>
					<div class="uk-width-1-3 uk-padding-small" style="margin-top:0;padding-top:0">
						Slider 1
						<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon1.' uk-text-muted pointer" data-tabla="portafolio" data-campo="slider1" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['slider1'].'"></i>
					</div>
					<div class="uk-width-1-3 uk-padding-small" style="margin-top:0;padding-top:0">
						Slider 2
						<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon2.' uk-text-muted pointer" data-tabla="portafolio" data-campo="slider2" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['slider2'].'"></i>
					</div>
					<div class="uk-width-1-3 uk-padding-small" style="margin-top:0;padding-top:0">
						Slider 3
						<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon3.' uk-text-muted pointer" data-tabla="portafolio" data-campo="slider3" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['slider3'].'"></i>
					</div>
				</div>

				<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="portafolio" data-campo="estatus" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['estatus'].'"></i>
				<a href="'.$pic.'" class="uk-icon-button uk-button-default" target="_blank" uk-icon="icon:image"></a> &nbsp;
				<a href="javascript:eliminaPic(picID='.$prodID.')" class="uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></a>
				<br>
				<div style="padding-bottom: 50px;">
					<img src="'.$pic.'" class=" uk-border-rounded margin-top-20"><br>
				</div>
				<div class="padding-top-20 uk-position-bottom">
					<input type="text" class="editarajax uk-input" data-tabla="portafolio" data-id="'.$row_Consulta1['id'].'" data-campo="titulo"  data-valor="'.$row_Consulta1['titulo'].'" value="'.$row_Consulta1['titulo'].'" placeholder="Titulo">
					<input type="text" class="editarajax uk-input" data-tabla="portafolio" data-id="'.$row_Consulta1['id'].'" data-campo="url" data-valor="'.$row_Consulta1['url'].'" value="'.$row_Consulta1['url'].'" placeholder="Url">
				</div>
			</div>

		</div>';
	}else{
		echo '
		<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$prodID.'">
			<div class="uk-card uk-card-default uk-card-body uk-text-center">
				<a href="javascript:eliminaPic(picID='.$prodID.')" class="uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></a>
				<br>
				Imagen rota<br>
				<i uk-icon="icon:ban;ratio:2;"></i>
			</div>
		</div>';
	}
}


echo '	
	</div>
</div>


<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<div class="margin-v-50">
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>';


$scripts='
	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "png",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:true,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&position=gallery&imagen=\'+data);
			}
		});
	});
	

	function eliminaPic () { 
		var statusConfirm = confirm("Realmente desea eliminar esta foto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&borrarPic&id="+picID);
		} 
	};

	';

