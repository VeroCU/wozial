<?php
$CONSULTA = $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");
$rowCONSULTA = $CONSULTA -> fetch_assoc();

echo '
	<div class="uk-width-auto@m margin-top-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">Home</a></li>
		</ul>
	</div>';


echo '
	<div class="uk-width-1-1">
		<div class="uk-container uk-container-xsmall">
			<form action="index.php" class="uk-width-1-1" method="post" name="editar" onsubmit="return checkForm(this);">
				<div uk-grid class="uk-grid-small uk-child-width-1-3@l uk-child-width-1-2@m">
					<input type="hidden" name="editar" value="1">
						<input type="hidden" name="seccion" value="'.$seccion.'">
					<div class="uk-width-1-4">
						<label class="uk-text-capitalize" for="partner_titulo">Titulo</label>
						<input type="text" class="uk-input" name="partner_titulo" value ="'.$rowCONSULTA['partner_titulo'].'" required>
					</div>
					<div class="uk-width-1-4">
						<label class="uk-text-capitalize" for="partner_titulo_dorado">Titulo Dorado</label>
						<input type="text" class="uk-input" name="partner_titulo_dorado" value ="'.$rowCONSULTA['partner_titulo_dorado'].'"required>
					</div>
					<div class="uk-width-1-4">
						<label class="uk-text-capitalize" for="partner_subtitulo">Subtitulo</label>
						<input type="text" class="uk-input" name="partner_subtitulo" value ="'.$rowCONSULTA['partner_subtitulo'].'">
					</div>
					<div class="uk-width-1-4">
						<label class="uk-text-capitalize" for="partner_subtitulo_dorado">Subtitulo Dorado</label>
						<input type="text" class="uk-input" name="partner_subtitulo_dorado" value ="'.$rowCONSULTA['partner_subtitulo_dorado'].'">
					</div>
					<div class="uk-width-1-1">
						<div class="margen-top-20">
							<label for="txt">Descripción</label>
							<textarea class="editor" name="partner_detalle">'.$rowCONSULTA['partner_detalle'].'</textarea>
						</div>
					</div>

					<div class="uk-width-1-1 uk-text-center">s
						<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
						<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	';




