<?php
$CONSULTA = $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");
$rowCONSULTA = $CONSULTA -> fetch_assoc();

echo '
<div class="uk-width-auto@m margin-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Configuración</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'" class="color-red">Nosotros</a></li>
	</ul>
</div>




<div class="uk-width-1-1">
	<div class="uk-container">

		<form action="index.php" method="post">
			<div uk-grid class="uk-grid-large">
				<input type="hidden" name="seccion" value="'.$seccion.'">
				<input type="hidden" name="editartextosconformato" value="1">
				<input type="hidden" name="subseccion" value="about">

				<div class="uk-width-1-2">
					<label class="uk-text-capitalize" for="about_titulo">Titulo</label>
					<input type="text" class="uk-input" name="about_titulo" value="'.$rowCONSULTA['about_titulo'].'">
				</div>
				<div class="uk-width-1-2">
					<label class="uk-text-capitalize" for="about_titulo_dorado">Titulo Dorado</label>
					<input type="text" class="uk-input" name="about_titulo_dorado" value="'.$rowCONSULTA['about_titulo_dorado'].'">
				</div>
				<div class="uk-width-1-3@l margin-v-50 uk-text-left">
					<div class="uk-width-1-1 uk-padding-small">
						<label class="uk-text-capitalize" for="about_subtitulo1">Subtitulo</label>
						<input type="text" class="uk-input" name="about_subtitulo1" value="'.$rowCONSULTA['about_subtitulo1'].'">
					</div>
					<textarea class="editor min-height-150" name="about1">'.$rowCONSULTA['about1'].'</textarea>
						<br>
				</div>

				<div class="uk-width-1-3@l margin-v-50 uk-text-left">
					<div class="uk-width-1-1 uk-padding-small">
						<label class="uk-text-capitalize" for="about_subtitulo2">Subtitulo</label>
						<input type="text" class="uk-input" name="about_subtitulo2" value="'.$rowCONSULTA['about_subtitulo2'].'">
					</div>
					
						<textarea class="editor min-height-150" name="about2">'.$rowCONSULTA['about2'].'</textarea>
						<br>
				</div>

				<div class="uk-width-1-3@l margin-v-50 uk-text-left">
					<div class="uk-width-1-1 uk-padding-small">
						<label class="uk-text-capitalize" for="about_subtitulo3">Subtitulo</label>
						<input type="text" class="uk-input" name="about_subtitulo3" value="'.$rowCONSULTA['about_subtitulo3'].'">
					</div><textarea class="editor min-height-150" name="about3">'.$rowCONSULTA['about3'].'</textarea>
						<br>
				</div>

				<div uk-grid>
					<div class="uk-text-center">
						<button class="uk-button uk-button-primary">Guardar</button>
					</div>
				</div>
			</div>
		</form>
	</div>';


echo '
	</div>
</div>
';

