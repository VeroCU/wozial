<?php 
echo '
<div class="uk-width-1-2 margen-top-20 uk-text-left">
	<ul class="uk-breadcrumb">
		<li><a href="index.php?seccion='.$seccion.'" class="color-red">'.$seccion.'</a></li>
	</ul>
</div>
';
?>

<div class="uk-width-1-1 margen-top-20">
	<div class="uk-container uk-container-xsmall">
		<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive uk-text-center" id="ordenar">
			<thead>
				<tr>
					<th onclick="sortTable(0)">Título</th>
					<th width="120px"></th>
				</tr>
			</thead>
			<tbody class="sortable" data-tabla="<?=$seccion?>">

				<?php 
				$fileRuta="../img/contenido/".$seccion."/";

				$consulta = $CONEXION -> query("SELECT * FROM $seccion ORDER BY orden");
				while ($rowConsulta = $consulta -> fetch_assoc()) {
					$id=$rowConsulta['id'];

					echo '
					<tr id="'.$id.'">
						<td>
							<input class="uk-input uk-form-blank editarajax" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$rowConsulta['id'].'" placeholder="Título" value="'.$rowConsulta['titulo'].'" tabindex="2">
						</td>
						<td>
							<a href="javascript:eliminaCatalogo('.$rowConsulta['id'].')" class="uk-icon-button uk-button-danger" uk-icon="icon:trash;"></a>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="index.php?rand='.rand(1,99999).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="uk-icon-button uk-button-primary" uk-icon="search"></a>
						</td>
					</tr>';
				}
				?>

			</tbody>
		</table>
	</div>
</div>



<div id="add" uk-modal class="modal">
    <div class="uk-modal-dialog uk-modal-body">
		<h3>Nuevo catálogo</h3>
		<form action="index.php" method="post">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="nuevocatalogo" value="1">
			<div class="uk-margin">
				<label for="titulo">Título</label>
				<input class="uk-input" type="text" name="titulo" tabindex="19">
			</div>
			<div class="uk-margin uk-text-center">
				<button class="uk-button uk-button-primary" tabindex="19">Guardar</button>
			</div>
		</form>
	</div>
</div>

<div id="upload" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-outside" type="button" uk-close></button>
		<input type="hidden" id="itemid">
		<input type="hidden" id="itemcampo">
		<div id="fileuploader">
			Cargar
		</div>
    </div>
</div>

<div>
	<div id="buttons">
		<a href="#add" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;" uk-toggle></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>

<?php
$scripts='
	function eliminaCatalogo (id) {
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true){
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarcatalogo: 1,
					id: id
				}
			})
			.done(function( response ) {
				console.log( response )
				UIkit.notification.closeAll();
				UIkit.notification( response );
				$("#"+id).addClass( "uk-invisible" );
			});
		}
	};


	';
?>