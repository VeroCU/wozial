<?php
$seccion='catalogos';
$seccionpic=$seccion.'pic';
$rutaFinal='../img/contenido/catalogos/';

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo catálogo    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevocatalogo']) AND isset($_POST['titulo'])){
		$titulo	= trim(htmlentities($_POST['titulo']));

		$sql = "INSERT INTO $seccion (titulo) VALUES ('$titulo')";
		if($insertar = $CONEXION->query($sql)){
			$id = $CONEXION->insert_id;
			$legendSuccess.= "<br> Agregado";
			$exito='success';
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo='danger';  
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar catálogo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['borrarcatalogo'])){
		include '../../../includes/connection.php';
		$rutaFinal='../../../img/contenido/catalogos/';
		$mensajeClase  = 'danger';
		$mensajeIcon   = 'ban';
		$mensaje       = 'No se pudo borrar';

		$id = (isset($_POST['id']))?$_POST['id']:'';

		$sql="SELECT * FROM $seccionpic WHERE producto = $id";
		//echo $sql;
		$CONSULTA = $CONEXION -> query($sql);
		while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
			$item=$rowCONSULTA['id'];


			// Borramos el archivo de imagen
			$filehandle = opendir($rutaFinal); // Abrir archivos
			while ($file = readdir($filehandle)) {
				if ($file != "." && $file != "..") {
					// Id de la imagen
					if (strpos($file,'-')===false) {
						$imagenID = strstr($file,'.',TRUE);
					}else{
						$imagenID = strstr($file,'-',TRUE);
					}
					// Comprobamos que sean iguales
					if($imagenID==$id){
						$pic=$rutaFinal.$file;
						unlink($pic);
					}
				}
			}
		}

		if($borrar = $CONEXION->query("DELETE FROM $seccion WHERE id = $id")){
			$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE producto = $id");
			$mensajeClase = 'success';
			$mensajeIcon  = 'check';
			$mensaje      = 'Borrado';
		}
		echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Imagen     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['borrarfoto'])){
		include '../../../includes/connection.php';
		$rutaFinal='../../../img/contenido/catalogos/';
		$mensajeClase  = 'danger';
		$mensajeIcon   = 'ban';
		$mensaje       = 'No se pudo borrar';

		$file = $_POST['file'];
		
		$id = strstr($file, '.', true);		

		// Borramos de la base de datos
		if($borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE id = $id")){
			$legendSuccess.= "<br>Catálogo eliminado";
			$exito='success';
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo='danger';  
		}
		// Borramos el archivo de imagen
		$filehandle = opendir($rutaFinal); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != "..") {
				// Id de la imagen
				if (strpos($file,'-')===false) {
					$imagenID = strstr($file,'.',TRUE);
				}else{
					$imagenID = strstr($file,'-',TRUE);
				}
				// Comprobamos que sean iguales
				if($imagenID==$id){
					$pic=$rutaFinal.$file;
					unlink($pic);
					$mensajeClase = 'success';
					$mensajeIcon  = 'check';
					$mensaje      = 'Borrado';
				}
			}
		}
		closedir($filehandle);
		echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir foto     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_FILES["uploadedfile"])){
		include '../../../includes/connection.php';
		$rutaFinal = '../../../img/contenido/'.$seccion.'/';
		$id=$_GET['id'];

		$ret = array();
		
		$error =$_FILES["uploadedfile"]["error"];
		//You need to handle  both cases
		//If Any browser does not support serializing of multiple files using FormData() 
		if(!is_array($_FILES["uploadedfile"]["name"])) //single file
		{
	 	 	$archivoInicial = $_FILES["uploadedfile"]["name"];
			$i = strrpos($archivoInicial,'.');
			$l = strlen($archivoInicial) - $i;
			$ext = strtolower(substr($archivoInicial,$i+1,$l));

			if ($ext=='jpg' OR $ext=='jpeg') {
				$sql = "INSERT INTO $seccionpic (producto) VALUES ('$id')";
				if($insertar = $CONEXION->query($sql)){
					$idPic = $CONEXION->insert_id;
			 	 	$archivoFinal = $idPic.'.'.$ext;
			 		move_uploaded_file($_FILES["uploadedfile"]["tmp_name"],$rutaFinal.$archivoFinal);
			    	$ret[]= $archivoFinal;
			    }
			}
		}
		else  //Multiple files, file[]
		{
			$fileCount = count($_FILES["uploadedfile"]["name"]);
			for($i=0; $i < $fileCount; $i++){
				$archivoInicial = $_FILES["uploadedfile"]["name"][$i];
				$i = strrpos($archivoInicial,'.');
				$l = strlen($archivoInicial) - $i;
				$ext = strtolower(substr($archivoInicial,$i+1,$l));

				if ($ext=='jpg' OR $ext=='jpeg') {
					$sql = "INSERT INTO $seccionpic (producto) VALUES ('$id')";
					if($insertar = $CONEXION->query($sql)){
						$idPic = $CONEXION->insert_id;
						$archivoFinal = $idPic.'.'.$ext;
						move_uploaded_file($_FILES["uploadedfile"]["tmp_name"][$i],$rutaFinal.$archivoFinal);
						$ret[]= $archivoFinal;
					}
				}
			}
		}
	    echo json_encode($ret);
	}









