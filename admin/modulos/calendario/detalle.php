
<?php 
$CONSULTA = $CONEXION -> query("SELECT * FROM calendario WHERE id = $id");
$row_CONSULTA = $CONSULTA -> fetch_assoc();

echo '
<div class="uk-width-1-1">
	<div>
		<div class="uk-width-1-1">
			<ul class="uk-breadcrumb margen-v-20">
				<li><a href="index.php?seccion='.$seccion.'">eventos</a></li>
				<li><a href="index.php?rand='.rand(1,10000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$row_CONSULTA['txt'].'</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="uk-width-1-1">
	<div class="uk-card uk-card-default uk-card-body">
		<form action="index.php" method="post">
			<h3>Descripcion del evento</h3>
			<input type="hidden" name="seccion" value="calendario">
			<input type="hidden" name="subseccion" value="detalle">
			<input type="hidden" name="editar" value="1">
			<input type="hidden" name="id" value="'.$id.'">

			<div class="uk-margin">
				<label>Nombre del evento</label>
				<input type="text" class="uk-input" name="txt" value="'.$row_CONSULTA['txt'].'">
			</div>

			<div class="uk-margin">
				<label>Link del botón</label>
				<input type="text" class="uk-input" name="link" value="'.$row_CONSULTA['link'].'">
			</div>

			<div class="uk-margin">
				<label>Link del Mapa</label>
				<input type="text" class="uk-input" name="mapa" value="'.$row_CONSULTA['mapa'].'">
			</div>

			<div class="uk-margin">
				<label>Descripción del evento</label>
				<textarea class="editor min-height-150" name="txt1">'.$row_CONSULTA['txt1'].'</textarea>
			</div>
			
			<div class="uk-margin uk-text-center">
				<button class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</form>
	</div>
</div>';


// Fotografías
echo '
<div class="uk-width-1-1">
	<h3 class="uk-text-center">Fotografías</h3>
</div>

<div class="uk-width-1-1@m">
	<div>
		<div id="fileuploader">
			Cargar
		</div>
	</div>
</div>

<div class="uk-width-1-1 uk-text-center">
	<div uk-grid class="uk-grid-small uk-grid-match sortable" data-table="calendariopic" id="sortable2">';

$consultaPIC = $CONEXION -> query("SELECT * FROM calendariopic WHERE producto = $id ORDER BY orden,id");
$numProds=$consultaPIC->num_rows;
while ($row_consultaPIC = $consultaPIC -> fetch_assoc()) {

	$pic='../img/contenido/calendario/'.$row_consultaPIC['id'].'.jpg';

	if(file_exists($pic)){
		echo '
		<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$row_consultaPIC['id'].'">
			<div class="uk-card uk-card-default uk-card-body uk-text-center">
				<button data-id="'.$row_consultaPIC['id'].'" class="eliminapic uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></button>
				<br>
				<img src="'.$pic.'" class="img-responsive uk-border-rounded margen-top-20"><br>
				'.$row_consultaPIC['titulo'].'
			</div>
		</div>';
	}else{
		echo '
		<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$row_consultaPIC['id'].'">
			<div class="uk-card uk-card-default uk-card-body uk-text-center">
				<button data-id="'.$row_consultaPIC['id'].'" class="eliminapic uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></button>
				<br><br><br>
				Imagen rota<br><br><br>
				<i uk-icon="icon:warning;ratio:5;"></i>
			</div>
		</div>';
	}
}

	echo '
	</div>
</div>';




echo '
<div class="margen-v-50">
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>';


$scripts='

	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion=calendario&subseccion=detalle&id='.$id.'&position=gallery&imageupload=\'+data);
	
			}
		});
	});	

	// Eliminar foto
	$(".eliminapic").click(function() {
		var id = $(this).attr(\'data-id\');
		UIkit.modal.confirm("Desea eliminar esta imagen?").then(function() {
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&eliminapic=1&picid="+id);
		});
	});

';
