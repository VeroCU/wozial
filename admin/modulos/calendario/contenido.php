<?php
echo '
<div class="uk-width-1-2@s margen-top-20">
	<ul class="uk-breadcrumb">
		<li><a href="index.php?rand='.rand(1,2000).'&seccion='.$seccion.'" class="color-red">Calendario</a></li>
	</ul>
</div>

<div class="uk-width-1-2@s margen-top-20 uk-text-right">
	<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=folder" class="uk-button uk-button-primary"><i uk-icon="folder"></i> &nbsp; Archivo</a>
</div>


<div class="uk-width-1-1 padding-v-50">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th onclick="sortTable(0)" width="150px;">Fecha</th>
				<th onclick="sortTable(1)">Titulo</th>
				<th width="140px"></th>
			</tr>
		</thead>
		<tbody>
			<form action="index.php" method="post">
				<input type="hidden" name="seccion" value="'.$seccion.'">
				<input type="hidden" name="nuevoevento" value="1">
				<tr>
					<td>
						<input class="uk-input" type="date" name="fecha" tabindex="19">
					</td>
					<td>
						<input class="uk-input" type="text" name="txt" placeholder="Nuevo evento" tabindex="19">
					</td>
					<td>
						<button class="uk-icon-button uk-button-primary" uk-icon="plus" tabindex="19"></button>
					</td>
				</tr>
			</form>';

$Consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE folder = 1 ORDER BY fecha DESC");
while ($row_Consulta = $Consulta -> fetch_assoc()) {
	echo '
			<tr id="row'.$row_Consulta['id'].'">
				<td>
					<input value="'.$row_Consulta['fecha'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="fecha" data-id="'.$row_Consulta['id'].'" type="date">
				</td>
				<td>
					<input value="'.$row_Consulta['txt'].'"   class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="txt"   data-id="'.$row_Consulta['id'].'">
				</td>
				<td class="uk-text-right">
					<a href="index.php?rand='.rand(1,10000).'&seccion='.$seccion.'&subseccion=detalle&id='.$row_Consulta['id'].'" class="uk-icon-button uk-button-primary" uk-icon="search"></a> &nbsp;
					<button data-id="'.$row_Consulta['id'].'" class="folder uk-icon-button uk-button-white" uk-icon="folder"></button> &nbsp;
					<button data-id="'.$row_Consulta['id'].'" class="borrar uk-icon-button uk-button-danger" uk-icon="trash"></button>
				</td>
			</tr>';

}

echo '
		</tbody>
	</table>
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>
';


$scripts='
	$(".borrar").click(function() {
		var id = $(this).attr("data-id");
		UIkit.modal.confirm("Realmente desea borrar este evento?").then(function() {
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarevento: 1,
					id, id
				}
			})
			.done(function( msg ) {
				UIkit.notification.closeAll();
				UIkit.notification(msg);
				$("#row"+id).fadeOut( "slow" );
			});			
		}, function () {
			console.log("Rechazado")
		});
	});

	$(".folder").click(function() {
		var id = $(this).attr("data-id");
		$.ajax({
			method: "POST",
			url: "modulos/'.$seccion.'/acciones.php",
			data: { 
				archivar: 1,
				folder: 0,
				id: id
			}
		})
		.done(function( msg ) {
			UIkit.notification.closeAll();
			UIkit.notification(msg);
			$("#row"+id).fadeOut( "slow" );
		});
	});
	';
