<?php
echo '
<div class="uk-width-1-1 margen-top-20">
	<ul class="uk-breadcrumb">
		<li><a href="index.php?rand='.rand(1,2000).'&seccion='.$seccion.'">Calendario</a></li>
		<li><a href="index.php?rand='.rand(1,2000).'&seccion='.$seccion.'&subseccion=folder" class="color-red">Archivo</a></li>
	</ul>
</div>


<div class="uk-width-1-1 padding-v-50">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle">
		<thead>
			<tr class="uk-text-muted">
				<th onclick="sortTable(0)" width="150px;">Fecha</th>
				<th onclick="sortTable(1)">Titulo</th>
				<th width="100px"></th>
			</tr>
		</thead>';

$Consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE folder = 0 ORDER BY fecha DESC");
while ($row_Consulta = $Consulta -> fetch_assoc()) {
	echo '
			<tr id="row'.$row_Consulta['id'].'">
				<td>
					<input value="'.$row_Consulta['fecha'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="fecha" data-id="'.$row_Consulta['id'].'" type="date">
				</td>
				<td>
					<input value="'.$row_Consulta['txt'].'"   class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="txt"   data-id="'.$row_Consulta['id'].'">
				</td>
				<td>
					<span data-id="'.$row_Consulta['id'].'" class="folder uk-icon-button uk-button-white" uk-icon="folder"></span> &nbsp;
					<span data-id="'.$row_Consulta['id'].'" class="borrar uk-icon-button uk-button-danger" uk-icon="trash"></span>
				</td>
			</tr>';

}

echo '
		</tbody>
	</table>
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>
';


$scripts='

	// Eliminar foto
	$(".eliminapic").click(function() {
		var id = $(this).attr(\'data-id\');
		UIkit.modal.confirm("Desea eliminar esto?").then(function() {
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&borrar=1&id="+id);
		});
	});

	var imagenesArray = [];
	$("#fileuploader").uploadFile({
		url:"../library/upload-file/php/upload.php",
		fileName:"myfile",
		maxFileCount:1,
		showDelete: "false",
		allowedTypes: "jpeg,jpg",
		maxFileSize: 6291456,
		showFileCounter: false,
		showPreview:false,
		returnType:"json",
		onSuccess:function(data){ 
			window.location = ("index.php?seccion='.$seccion.'&imagen="+data);
		}
	});

	$(".borrar").click(function() {
		var id = $(this).attr("data-id");
		UIkit.modal.confirm("Realmente desea borrar este evento?").then(function() {
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarevento: 1,
					id: id
				}
			})
			.done(function( msg ) {
				UIkit.notification.closeAll();
				UIkit.notification(msg);
				$("#row"+id).fadeOut( "slow" );
			});			
		}, function () {
			console.log("Rechazado")
		});
	});

	$(".folder").click(function() {
		var id = $(this).attr("data-id");
		$.ajax({
			method: "POST",
			url: "modulos/'.$seccion.'/acciones.php",
			data: { 
				archivar: 1,
				folder: 1,
				id: id
			}
		})
		.done(function( msg ) {
			UIkit.notification.closeAll();
			UIkit.notification(msg);
			$("#row"+id).fadeOut( "slow" );
		});
	});

	';
