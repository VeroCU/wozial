<?php
$CONSULTA = $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");
$rowCONSULTA = $CONSULTA -> fetch_assoc();

echo '
	<div class="uk-width-auto@m margin-top-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">Home</a></li>
		</ul>
	</div>';


echo '
	<div class="uk-width-1-1">
		<div class="uk-container uk-container">
			<div uk-grid class="uk-child-width-1-2@m">
				<form action="index.php" class="uk-width-1-1 uk-padding-small" method="post" name="editar" onsubmit="return checkForm(this);">
					<input type="hidden" name="editar" value="1">
						<input type="hidden" name="seccion" value="'.$seccion.'">
						<div class="uk-padding-small " style="margin-left: 0" uk-grid>
							<div class="uk-width-1-2">
								<label class="uk-text-capitalize" for="titulo">Titulo</label>
								<input type="text" class="uk-input" name="titulo" value ="'.$rowCONSULTA['titulo'].'" required>
							</div>
							<div class="uk-width-1-2">
								<label class="uk-text-capitalize" for="titulo_dorado">Titulo Dorado</label>
								<input type="text" class="uk-input" name="titulo_dorado" value ="'.$rowCONSULTA['titulo_dorado'].'"required>
							</div>
							<div class="uk-width-1-2">
								<label class="uk-text-capitalize" for="subtitulo">Subtitulo</label>
								<input type="text" class="uk-input" name="subtitulo" value ="'.$rowCONSULTA['subtitulo'].'">
							</div>
							<div class="uk-width-1-2">
								<label class="uk-text-capitalize" for="subtitulo_dorado">Subtitulo Dorado</label>
								<input type="text" class="uk-input" name="subtitulo_dorado" value ="'.$rowCONSULTA['subtitulo_dorado'].'">
							</div>
							<div class="uk-width-1-2">
								<label class="uk-text-capitalize" for="titulo_ing">Titulo en Ingles</label>
								<input type="text" class="uk-input" name="titulo_ing" value ="'.$rowCONSULTA['titulo_ing'].'" required>
							</div>
							<div class="uk-width-1-2">
								<label class="uk-text-capitalize" for="titulo_dorado_ing">Titulo Dorado en Ingles</label>
								<input type="text" class="uk-input" name="titulo_dorado_ing" value ="'.$rowCONSULTA['titulo_dorado_ing'].'"required>
							</div>
							<div class="uk-width-1-2">
								<label class="uk-text-capitalize" for="subtitulo_ing">Subtitulo en Ingles</label>
								<input type="text" class="uk-input" name="subtitulo_ing" value ="'.$rowCONSULTA['subtitulo_ing'].'">
							</div>
							<div class="uk-width-1-2">
								<label class="uk-text-capitalize" for="subtitulo_dorado_ing">Subtitulo Dorado en Ingles</label>
								<input type="text" class="uk-input" name="subtitulo_dorado_ing" value ="'.$rowCONSULTA['subtitulo_dorado_ing'].'">
							</div>
							<div class="uk-width-1-2">
								<div class="margen-top-20">
									<label for="txt">Descripción</label>
									<textarea class="editor" name="detalle">'.$rowCONSULTA['detalle'].'</textarea>
								</div>
							</div>
							<div class="uk-width-1-2">
								<div class="margen-top-20">
									<label for="detalle_ing">Descripción en Ingles</label>
									<textarea class="editor" name="detalle_ing">'.$rowCONSULTA['detalle_ing'].'</textarea>
								</div>
							</div>
						</div>
					

					<div class="uk-width-1-1 uk-text-center">
						<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
						<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	';




