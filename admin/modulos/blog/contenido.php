
<div class="uk-width-1-2 margen-top-20 uk-text-left">
	<ul class="uk-breadcrumb">
		<?php 
		echo '
		<li><a href="index.php?seccion='.$seccion.'" class="color-red">'.$seccion.'</a></li>
		';
		?>
	</ul>
</div>


<div class="uk-width-medium-1-1 margen-v-50">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-middle uk-tablle-responsive">
		<thead>
			<tr class="uk-text-muted">
				<th >Título</th>
				<th width="120px">Primera foto</th>
				<th width="100px"></th>
			</tr>
		</thead>
		<tbody class="sortable" data-tabla="blog">
		<?php
		$blog = $CONEXION -> query("SELECT * FROM blog ORDER BY orden");
		while ($row_blog = $blog -> fetch_assoc()) {

			$prodID=$row_blog['id'];

			$picROW='<img src="../img/design/blank.png" class="uk-border-rounded" width="100px">';
			$blogpic = $CONEXION -> query("SELECT * FROM blogpic WHERE item = $prodID ORDER BY orden");
			$row_blogpic = $blogpic -> fetch_assoc();
			$pic='../img/contenido/blog/'.$row_blogpic['id'].'-nat500.jpg';
			if(file_exists($pic)){
				$picROW='<img src="'.$pic.'" class="uk-border-rounded" width="100px">';
			}

			$link='index.php?seccion=blog&subseccion=detalle&id='.$row_blog['id'];

			echo '
			<tr id="'.$row_blog['id'].'">
				<td>
					'.$row_blog['titulo'].'
				</td>
				<td>
					'.$picROW.'
				</td>
				<td class="uk-text-center">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"  uk-icon="icon:pencil"></i></a>
					<a href="javascript:eliminaProd(id='.$row_blog['id'].')" class="uk-icon-button uk-button-danger" uk-icon="icon:trash"></i></a> 
				</td>
			</tr>';
		$picROW='';
		}
		?>

		</tbody>
	</table>
</div>

<div>
	<div id="buttons">
		<a href="index.php?seccion=<?=$seccion?>&subseccion=nuevo" id="add-button" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon: plus;ratio:1.4"></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>

<?php
$scripts='
	// Eliminar producto
	function eliminaProd () { 
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&borrarPod&id="+id);
		} 
	};'

?>