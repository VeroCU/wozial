<?php
$catalogo = $CONEXION -> query("SELECT * FROM blog WHERE id = $id");
$row_catalogo = $catalogo -> fetch_assoc();

echo '
	<div class="uk-width-1-2@m margen-top-20">
		<ul class="uk-breadcrumb">
			<li><a href="index.php?seccion='.$seccion.'">'.$seccion.'</a></li>
			<li><a href="index.php?seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$row_catalogo['titulo'].'</a></li>
		</ul>
	</div>';

echo '
	<div class="uk-width-1-1 margen-v-20">
		<form action="index.php" method="post" name="datos" onsubmit="return checkForm(this);">
			<input type="hidden" name="editar" value="1">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="subseccion" value="detalle">
			<input type="hidden" name="id" value="'.$id.'">
			<div class="uk-margin margen-top-20 uk-width-1-1">
				<label for="titulo">Título</label>
				<input type="text" class="uk-input" name="titulo" value="'.$row_catalogo['titulo'].'" required>
			</div>
			<div class="uk-margin margen-top-20 uk-width-1-1">
				<label for="video">video de youtube</label>
				<input type="text" class="uk-input" name="video" value="'.$row_catalogo['video'].'" >
			</div>
			<div class="uk-margin margen-top-20 uk-width-1-1">
				<label for="txt">Descripción</label>
				<textarea class="editor" name="txt">'.$row_catalogo['txt'].'</textarea>
			</div>
			<div class="uk-width-1-1 uk-text-center margen-top-20">
				<input type="submit" name="send" value="Guardar" class="uk-button uk-button-primary uk-button-large">
			</div>
		</form>
	</div>';


	if (strlen($row_catalogo['video'])>0) {
		$videoUrl=$row_catalogo['video'];
		$videoPic=$videoUrl;
		if (strpos($videoPic, 'youtube')) {
			$pos=strpos($videoPic, 'v');
			$videoPic=substr($videoPic, ($pos+2));
		}elseif (strpos($videoPic, 'youtu.be')) {
			$pos=strrpos($videoPic, '/');
			$videoPic=substr($videoPic, ($pos+1));
		}
		$pic='https://img.youtube.com/vi/'.$videoPic.'/0.jpg';
		$play='<img src="../img/design/play.png" class="uk-width-1-1 uk-position-absolute" style="top:0;left:0;margin-top:-25%;">';
		echo '
				<div>
					<div class="margen-v-20">
						<a href="'.$videoUrl.'" target="_blank" class="uk-position-relative">
							<img src="'.$pic.'" class=" max-height-300px">
							'.$play.'
						</a>
					</div>
				</div>';
	}

echo '
	<div class="uk-width-1-1 margen-top-50">
		<span class="uk-text-muted">
			Para ordenar fotos arrastre y suelte.<br>
		</span>
		<div id="fileuploader">
			Cargar
		</div>
		<br><span id="msg" class="color-red">&nbsp;</span>
	</div>

	<div class="uk-width-1-1">
		<div uk-grid class="sortable uk-grid-match uk-grid-small" data-tabla="blogpic">';
	$num=1;
	$picTXT='';
	$productosPIC = $CONEXION -> query("SELECT * FROM blogpic WHERE item = $id ORDER BY orden,id");
	while ($row_productosPIC = $productosPIC -> fetch_assoc()) {

		$pic='../img/contenido/'.$seccion.'/'.$row_productosPIC['id'].'-nat500.jpg';
		$picCopy='../img/contenido/'.$seccion.'/'.$row_productosPIC['id'].'-nat1000.jpg';
		if(file_exists($pic)){
			echo '
			<div id="'.$row_productosPIC['id'].'" class="uk-width-1-4@l uk-width-1-3@m uk-width-1-2">
				<div class="uk-card uk-card-default uk-card-body uk-text-center">
					&nbsp;
					<a href="javascript:eliminaPic(picID='.$row_productosPIC['id'].')" class="uk-icon-button uk-button-danger" uk-icon="icon:trash;"></a>
					<br><br>
					<img src="'.$pic.'" class="uk-border-rounded margen-bottom-20">
				</div>
			</div>';
		}else{
			echo '
			<div class="uk-width-1-5 uk-text-center" id="'.$row_productosPIC['id'].'">
				<p class="uk-scrollable-box"><i uk-icon="icon:chain-broken uk-icon-large"></i><br><br>
					Imagen rota<br><br>
					<a href="javascript:eliminaPic(picID='.$row_productosPIC['id'].')" class="uk-icon-button uk-button-danger" tabindex="1" uk-icon="trash"></a>
				</p>
			</div>';
		}
	}

	echo '
		</div>
	</div>

	<div>
		<div id="buttons">
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>
	';


$scripts='
	// Eliminar producto
	function eliminaProd () { 
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion=<?=$seccion?>&borrarPod&id="+id);
		} 
	};

	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&imagen=\'+data);
			}
		});
	});

	// Eliminar foto
	function eliminaPic () { 
		console.log(picID);
		var statusConfirm = confirm("Realmente desea eliminar esta foto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&borrarPic&id='.$id.'&picID="+picID);
		} 
	};

';

