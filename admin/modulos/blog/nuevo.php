<?php
echo'
<div class="uk-width-1-3@m margen-top-20">
	<ul class="uk-breadcrumb">
		<li><a href="index.php?seccion='.$seccion.'">'.$seccion.'</a></li>
		<li><a href="index.php?seccion='.$seccion.'&subseccion=nuevo" class="color-red">Nuevo</a></li>
	</ul>
</div>';
?>

<form action="index.php" class="uk-width-1-1" method="post" name="editar" onsubmit="return checkForm(this);">

	<div class="uk-container uk-container-small">

		<input type="hidden" name="nuevo" value="1">
		<input type="hidden" name="seccion" value="<?=$seccion?>">
		<input type="hidden" name="subseccion" value="detalle">
	
		<div class="uk-margin">
			<label for="titulo">Título</label>
			<input type="text" class="uk-input" name="titulo" required autofocus>
		</div>
		<div class="uk-margin">
			<label for="video">video de youtube</label>
			<input type="text" class="uk-input" name="video">
		</div>
		<div class="uk-margin">
			<label for="txt">Descripción</label>
			<textarea class="editor" name="txt"></textarea>
		</div>
		<div class="uk-width-1-1 uk-text-center margen-top-20">
			<a href="index.php?seccion=<?=$seccion?>" class="uk-button uk-button-white" tabindex="10">Cancelar</a>
			<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
		</div>
	</div>
</form>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>
