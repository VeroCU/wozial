<?php 
$rutaPics='../img/contenido/profile/';
$mensajeConsulta = $CONEXION -> query("SELECT * FROM mensajes WHERE id = $id");
$rowMensaje = $mensajeConsulta -> fetch_assoc();
$fecha =date_create($rowMensaje["creado"]);
$fecha = date_format($fecha,"Y/m/d H:i");

$actualizar = $CONEXION->query("UPDATE mensajes SET leido = 1 WHERE id = $id");
$exito=1;

echo '
	<div class="uk-width-1-1 uk-section uk-section-muted">
		<div class="uk-container uk-container-large uk-margin-top">
			<div class="uk-grid" uk-grid>
				<h2>Mensaje</h2>
				
				<div class="uk-width-1-1 text-lg">
					<p><span>Nombre: </span>'.$rowMensaje["nombre"].'</p>
				</div>
				<div class="uk-width-1-1@s uk-width-1-4@m text-lg">
					<p><span>Correo: </span>'.$rowMensaje["email"].'</p>
				</div>
				<div class="uk-width-1-1@s uk-width-1-4@m text-lg">
					<p><span>Whatsapp: </span>'.$rowMensaje["whatsapp"].'</p>
				</div>
				<div class="uk-width-1-1@s uk-width-1-2@m text-lg">
					<p class="uk-text-left@s uk-text-right@m"><span>Fecha: </span>'.$fecha.'</p>
				</div>

			</div>
			<hr class="uk-divider-icon">
			<div class="uk-grid-small uk-padding-small" uk-grid>
				<div class="uk-width-1-1@s uk-width-1-1 uk-card uk-card-default uk-padding" style="min-height: 250px;">
					'.$rowMensaje["txt"].'
				</div>
			</div>	
';

	if($rowMensaje["llamada"]==1){	

echo '		<hr class="uk-divider-icon">
			<div class="uk-grid-small	 uk-padding-small" uk-grid>
				<div class="uk-width-1-1@s uk-width-1-2@må">
				<i class="fa fa-phone"></i>
					El cliente agendo llamada para el dia '.$rowMensaje["dia"].' a las '.$rowMensaje["hora"].'

				</div>
			</div>
	';
}

echo '
		</div>
	</div>
';


$scripts='
		$(document).ready(function(){
			$(".borrar").on("click", function (e) {
				var foto = $(this).attr("data-foto");

				UIkit.modal.confirm("Desea borrar esta foto?").then(function() {
					$.ajax({
						method: "POST",
						url: "modulos/'.$seccion.'/acciones.php",
						data: { 
							picdelete: 1,
							foto: foto,
							id: '.$id.'
						}
					})
					.done(function( msg ) {
						UIkit.notification.closeAll();
						UIkit.notification(msg);
						$("#piccliente").fadeOut();
					});
				}, function () {
					console.log("Rechazado")
				});
			});
		});


';