<?php 
echo '
<div class="uk-width-1-1 margen-v-20">
	<ul class="uk-breadcrumb">
		<li><a href="index.php?seccion='.$seccion.'" class="color-red">Mensajes.</a></li>
	</ul>
</div>

<div class="uk-width-1-1 margen-bottom-20">
	<table class="uk-table uk-table-hover uk-table-striped uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr>
				<th onclick="sortTable(1)" width="50px">ID</th>
				<th onclick="sortTable(2)">Nombre</th>
				<th onclick="sortTable(3)">Email</th>
				<th onclick="sortTable(4)" width="120px" class="uk-text-center">Whatsapp</th>
				<th onclick="sortTable(5)" width="180px" class="uk-text-center">Fecha</th>
				<th width="190px"></th>
			</tr>
		</thead>
		<tbody>';

		$consulta = $CONEXION -> query("SELECT * FROM mensajes");
		$numRows = $consulta ->num_rows;
		while($rowConsulta = $consulta -> fetch_assoc()){
			
			$negritas="";
			$id=$rowConsulta['id'];
			if($rowConsulta['leido'] == 0){
				$negritas = "negritas";
			}
			$link='index.php?seccion='.$seccion.'&subseccion=detalle&id='.$id;
			$numMensajes=$consulta->num_rows;

			$picDefault=$rutaFinal.'default.jpg';
			
			echo '
			<tr>
				<td class="'.$negritas.'">
					<span class="uk-hidden@m uk-text-muted">ID:</span>
					'.$id.'
				</td>
				<td class="'.$negritas.'">
					<span class="uk-hidden@m uk-text-muted">Nombre:</span>
					'.$rowConsulta['nombre'].'
				</td>
				<td class="'.$negritas.'">
					<span class="uk-hidden@m uk-text-muted">Email:</span>
					'.$rowConsulta['email'].'
				</td>
				<td class="'.$negritas.'">
					<span class="uk-hidden@m uk-text-muted">Whatsapp:</span>
					'.$rowConsulta['whatsapp'].'
				</td>
				
				<td class="uk-text-center@m  '.$negritas.'">
					<span class="uk-hidden@m uk-text-muted ">fecha:</span>
					'.$rowConsulta['creado'].'
				</td>
				<td class="uk-text-nowrap">
					<button data-id="'.$id.'" class="eliminamsg uk-icon-button uk-button-danger" uk-icon="icon:trash"></button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a>
				</td>
			</tr>';
		}

echo'
		</tbody>
	</table>
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>
';


$scripts='
	$(".eliminamsg").click(function() {
		console.log("siiii :D");
		var statusConfirm = confirm("Realmente desea eliminar este mensaje?");
		var id=$(this).data("id");
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&borrarMsg&id="+id);
		} 
	});';
	