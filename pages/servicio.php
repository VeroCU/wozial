<!DOCTYPE html>
<?=$headGNRL?>
<body>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

 <?php
 	$menu="";
 	// CONSULTAMOS LOS SERVICIOS(productos) POR ID
 	$consultaSer = $CONEXION -> query("SELECT * FROM productos WHERE id= $id");
 	$servicio = $consultaSer -> fetch_assoc();
 	$consultaServicios = $CONEXION -> query("SELECT * FROM productos WHERE estatus = 1 AND id != $id");
 	// CONSULTAMOS LOS PORTAFOLIO(proyectos) 
 	$urlPortafolios="./img/contenido/portafolio/";
 	$consultaPortafolios = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1");

 	while ($servicios = $consultaServicios -> fetch_assoc() ) {
		$menu.='
			<li>
				<a href="'.$servicios['id'].'_servicios.php">
					<div class="uk-width-1-1text-7"> '.$servicios['titulo'].' <span class="text-dorado">'.$servicios['titulo_dorado'].'</span> /</div>
				</a>
			</li>
		';
	}
	$consultamovil1 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1");
 ?> 

<section class="full-container uk-grid-collapse">
	<?=$header?>
	<div class="uk-grid-collapse left-0" uk-grid>
		<div class="uk-hidden@m margin-v-70"> &nbsp; </div>
	    
	    <?= $headerizq?>
	    
	    <div class="uk-width-expand@m uk-grid-collapse teleft-0 height-100">
	        <div class="uk-flex uk-flex-middle height-100">
	        	<div class="uk-width-1-1">
	        		<div class="uk-container uk-container-small ">
	        			<img class="uk-align-left bordes-0 pad-container fadeInDown-animate detail_img" src="./img/contenido/productosmain/<?= $servicio["imagen"] ?>">
	        		</div>
	        		<div class="uk-container uk-container-small">
		        		<div class="uk-grid-collapse pad-container">
		        			<hr class="hr-small">
		        			<h2 class="uk-text-left uk-grid-collapse margin-bottom-15">
			        			<?= $servicio["titulo"] ?> <span class="text-dorado"><?= $servicio["titulo_dorado"] ?></span> <br>
			        			<?= $servicio["subtitulo"] ?> <span class="text-dorado"><?= $servicio["subtitulo_dorado"] ?></span> 
			        		</h2>
		        			<p class="uk-text-left uk-grid-collapse uk-text-left p" style="color:#000;
			line-height:auto!important;">
		        				<?php echo $servicio["txt"] ?>
		        			</p>
		        			<h3 class="uk-text-left uk-grid-collapse left-0 margin-top-15 costo-animate"> <?= $servicio["precio"] ?> </h3>
		        			<hr class="hr-small">
		        		</div>
		        		<div class="uk-flex uk-flex-center uk-grid-collaps left-0" uk-grid>
						    <div class="uk-width-1-1@s uk-width-1-3@m uk-grid-collapse" style="margin-bottom:20px">
						        <a class="uk-button uk-button-default k-align-center uk-button-primary margin-left-10" href="#modal-center" uk-toggle>
						    		<p style="margin-top:14px">COTIZACIÓN</p>
						    	</a>
							</div>
						</div>
						<!--div class="uk-width-1-1@s uk-width-1-2@s margin-top-20 uk-grid-collapse position-submenu margin-contain">
							<div class="uk-grid-collapse uk-text-left left-0 margin-contain" style="color:#000;" uk-grid>
								<ul class="services">
									<?= $menu  ?>
								</ul>
							</div>
			        	</div-->
			        </div>
				</div>
	        </div>
	    </div>

	    
	    <?php echo $sliderderecha ?>

	    <?php echo $slidermovil ?>
	    
	    <?php echo $btnportafolio ?>

	</div>
	<?=$footer?>
</section>
<script type="text/javascript">
	$('.slick-carousel').slick({
	  autoplay:true,
	  infinite: true,
	  vertical:true,
	  verticalSwiping:true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  prevArrow: $('.top-arrow'),
	  nextArrow: $('.bottom-arrow')
	});
</script>


<?=$scriptGNRL?>


</body>
</html>