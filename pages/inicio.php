<!DOCTYPE html>
<?=$headGNRL?>

<?php
 	$menu="";
 	$urlPortafolios="./img/contenido/portafolio/";
 	// CONSULTAMOS LOS SERVICIOS(productos) POR ID
 	$consultaPortafolios = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1");
 	
 ?> 

<body>



<section class="full-container uk-grid-collapse">
	<?=$header?>
	<div class="uk-grid-collapse left-0" uk-grid>
		<div class="uk-hidden@m margin-v-70"> &nbsp; </div>
	    
	    <?= $headerizq?>

	    <div class="uk-width-expand@m uk-grid-collapse teleft-0 height-100">
            <div class="uk-flex uk-flex-middle height-100" >
                <div class="uk-width-1-1">
	        		<div class="uk-container uk-container-small pad-container">
	        			<h2 class="uk-text-left margin-bottom-15">
		        			<?= $titulo ?> <span style="color:#bf9d59"><?= $tituloD ?></span> / <br>
		        			<?= $subtitulo ?> <span style="color:#bf9d59"><?= $subtituloD ?></span>
		        		</h2>
	        			<p class="uk-text-left margin-top-30 p">
	        				<?= $detalle ?>
	        			</p>
	        		
	        		<div class="uk-child-width-1-4@m uk-grid-match uk-grid-collapse margin-top-20 left-0 movil-pad-40" uk-grid >
	        				<div></div>
						    <div class="left-0">
						    	<a href="#menu-movil" uk-toggle id="menu"
						    	class="uk-button uk-button-default k-align-center uk-button-primary margin-left-10 left-animate uk-align-center">
						    		<p style="margin-top:14px;padding-right:0;padding-left:0;width:auto!important;text-align:center!important;">VER SERVICIOS</p>
						    	</a>
						    </div>
						    <div class="left-0">
						    	<button class="uk-button uk-button-default uk-align-center uk-button-primary margin-left-10 right-animate" href="#modal-center" uk-toggle>
						    		<p style="margin-top:14px">COTIZACIÓN</p>
						    	</button>
						    </div>
						    <div></div>
					</div></div>
	        	</div>
	        </div>
	    </div>
	    
	    <?php echo $sliderderecha ?>

	    <?php echo $slidermovil ?>
	    
	    <?php echo $btnportafolio ?>
	</div>
	<?=$footer?>
</section>



<?=$scriptGNRL?>

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
	var altura = screen.width;
	$('.slick-carousel').slick({
	  autoplay:true,
	  infinite: true,
	  vertical:true,
	  verticalSwiping:true,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  prevArrow: $('.top-arrow'),
	  nextArrow: $('.bottom-arrow')
	});




</script>

</body>
</html>