<!DOCTYPE html>
<?=$headGNRL?>
<body>
<?php
	// CONSULTAMOS LOS PORTAFOLIO(proyectos) 
 	$urlPortafolios="./img/contenido/portafolio/";
 	$consultaPortafolios1 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1 AND slider1= 1");
 	$consultaPortafolios2 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1 AND slider2= 1");
 	$consultaPortafolios3 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1 AND slider3= 1");
 	$consultamovil1 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1 AND slider1= 1");
 	$consultamovil2 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1 AND slider2= 1");
 	$consultamovil3 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1 AND slider3= 1");
?>



<section class="full-container uk-grid-collapse">
	<?=$header?>
	<div class="uk-text-center uk-grid-collapse left-0" uk-grid>
		<div class="uk-hidden@m margin-v-70"> &nbsp; </div>
	    
	    <?= $headerizq?>
	    
	    <div class="uk-width-expand uk-flex uk-flex-bottom" style="">
	    	<div class="uk-width-1-1 left-0" uk-grid style="margin-left: 0; margin-top:30px">
	    		<div class="uk-width-1-4@m uk-grid-collapse uk-visible@m" style="height:90vh;"></div>
	    		
	    		<div class="uk-width-1-4@m uk-grid-collapse uk-visible@m" style="height:90vh;">
			        <div class="">
						<div class="slick-carousel1" style="height:90vh;">
							<?php 
								while ($portafolio = $consultaPortafolios1 -> fetch_assoc()) :
								$fondo = $urlPortafolios.$portafolio['id'].'.png';
							?>
							<div class="" style="height: calc(90vh / 3);">
						        <div class="uk-inline-clip uk-transition-toggle" tabindex="0" style="height:100%!important;width:100%">
						        	<?php  if($portafolio['url'] != ""):?>
							        <a class="uk-grid-collapse" href="<?= $portafolio['url'] ?>" target="_blank">
							        <?php endif ?>
								        <div class="uk-background-contain uk-background-muted uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" 
                              			style="height: calc(90vh / 3);width:calc(100vh / 3); background: url(<?=$fondo?>);background-position: bottom;background-repeat:no-repeat;background-size:100% 100%;">
                              				<?php  if($portafolio['url'] != ""):?>
								            <div class="uk-transition-fade uk-position-cover uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle">
								                <div class="uk-h4 uk-margin-remove uk-align-center">
								                	<img src="./img/design/logo-wozial.png" style="width:66px;margin-left:24px;">
								                	<p style="color:#fff; font-weight:400">Ver Proyecto</p>
								                </div>
								            </div>
								            <?php endif ?>
								        </div>
							        <?php  if($portafolio['url'] != ""):?>
							        </a>
							        <?php endif ?>
						    	</div>
					        </div>
					        <?php endwhile ?>
						</div>
			        </div>
			    </div>
			    <div class="uk-width-1-4@m uk-grid-collapse uk-visible@m" style="height:90vh;">
			        <div class="">
						<div class="slick-carousel2" style="height:90vh;">
							<?php 
								while ($portafolio2 = $consultaPortafolios2 -> fetch_assoc()) :
								$fondo = $urlPortafolios.$portafolio2['id'].'.png';
							?>
							<div class="" style="height: calc(90vh / 3);">
						        <div class="uk-inline-clip uk-transition-toggle" tabindex="0" style="height:100%!important;width:100%">
						        	<?php  if($portafolio2['url'] != ""):?>
							        <a class="uk-grid-collapse" href="<?= $portafolio2['url'] ?>" target="_blank">
							        <?php endif ?>
								        <div class="uk-background-contain uk-background-muted uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" 
                              			style="height: calc(90vh / 3);width:calc(100vh / 3); background: url(<?=$fondo?>);background-position: bottom;background-repeat:no-repeat;background-size:100% 100%;">
                              				<?php  if($portafolio2['url'] != ""):?>
								            <div class="uk-transition-fade uk-position-cover uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle">
								                <div class="uk-h4 uk-margin-remove uk-align-center">
								                	<img src="./img/design/logo-wozial.png" style="width:66px;margin-left:24px;">
								                	<p style="color:#fff; font-weight:400">Ver Proyecto</p>
								                </div>
								            </div>
								            <?php endif ?>
								        </div>
							        <?php  if($portafolio2['url'] != ""):?>
							        </a>
							        <?php endif ?>
						    	</div>
					        </div>
					        <?php endwhile ?>
						</div>
			        </div>
			    </div>
	    		<div class="uk-width-1-4@m uk-grid-collapse uk-visible@m" style="height:90vh;">
			        <div class="">
						<div class="slick-carousel3" style="height:90vh;">
							<?php 
								while ($portafolio3 = $consultaPortafolios3 -> fetch_assoc()) :
								$fondo = $urlPortafolios.$portafolio3['id'].'.png';
							?>
							<div class="" style="height: calc(90vh / 3);">
						        <div class="uk-inline-clip uk-transition-toggle" tabindex="0" style="height:100%!important;width:100%">
						        	<?php  if($portafolio3['url'] != ""):?>
							        <a class="uk-grid-collapse" href="<?= $portafolio3['url'] ?>" target="_blank">
							        <?php endif ?>
								        <div class="uk-background-contain uk-background-muted uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" 
                              			style="height: calc(90vh / 3);width:calc(100vh / 3); background: url(<?=$fondo?>);background-position: bottom;background-repeat:no-repeat;background-size:100% 100%;">
                              				<?php  if($portafolio3['url'] != ""):?>
								            <div class="uk-transition-fade uk-position-cover uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle">
								                <div class="uk-h4 uk-margin-remove uk-align-center">
								                	<img src="./img/design/logo-wozial.png" style="width:66px;margin-left:24px;">
								                	<p style="color:#fff; font-weight:400">Ver Proyecto</p>
								                </div>
								            </div>
								            <?php endif ?>
								        </div>
							        <?php  if($portafolio3['url'] != ""):?>
							        </a>
							        <?php endif ?>
						    	</div>
					        </div>
					        <?php endwhile ?>
						</div>
			        </div>
			    </div>

				<div class="uk-width-1-1 uk-hidden@m bordes-0" style="margin-top:80px">
			    	<div uk-slider="autoplay: true">
					    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
					        <ul class="uk-slider-items uk-child-width-1-1@s uk-child-width-1-3@m" >
					        	<?php  
					        		while ($portafolioMovil1 = $consultamovil1 -> fetch_assoc()) :
					        		$fondo = $urlPortafolios.$portafolioMovil1['id'].'.png';
					        	?> 
					        	<?php  if($portafolioMovil1['url'] != ""):?>
						        <a class="uk-grid-collapse" href="<?= $portafolioMovil1['url'] ?>" target="_blank">
						        <?php endif ?>
					            <li class="uk-flex uk-flex-center">
					                <div class="uk-background-contain uk-background-muted uk-height-medium uk-panel" 
		                              style="height: calc(100vh / 2);width:calc(100vh / 2); background: url('<?= $fondo ?>');background-position: bottom;background-repeat:no-repeat;background-size:100% 100%;">
		                            </div>
					            </li>
					            <?php  if($portafolioMovil1['url'] != ""):?>
					        	</a>
					            <?php endif ?>
								<?php endwhile  ?>
					        </ul>
					    </div>
					    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
					</div>
			    </div>
			    <div class="uk-width-1-1 uk-hidden@m bordes-0" style="margin-top:80px">
			    	<div uk-slider="autoplay: true">
					    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
					        <ul class="uk-slider-items uk-child-width-1-1@s uk-child-width-1-3@m" >
					        	<?php  
					        		while ($portafolioMovil2 = $consultamovil2 -> fetch_assoc()) :
					        		$fondo = $urlPortafolios.$portafolioMovil2['id'].'.png';
					        	?> 
					        	<?php  if($portafolioMovil2['url'] != ""):?>
						        <a class="uk-grid-collapse" href="<?= $portafolioMovil2['url'] ?>" target="_blank">
						        <?php endif ?>
					            <li class="uk-flex uk-flex-center">
					                <div class="uk-background-contain uk-background-muted uk-height-medium uk-panel" 
		                              style="height: calc(100vh / 2);width:calc(100vh / 2); background: url('<?= $fondo ?>');background-position: bottom;background-repeat:no-repeat;background-size:100% 100%;">
		                            </div>
					            </li>
					            <?php  if($portafolioMovil2['url'] != ""):?>
					        	</a>
					            <?php endif ?>
								<?php endwhile  ?>
					        </ul>
					    </div>
					    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
					</div>
			    </div>
			    <div class="uk-width-1-1 uk-hidden@m bordes-0" style="margin-top:80px">
			    	<div uk-slider="autoplay: true">
					    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
					        <ul class="uk-slider-items uk-child-width-1-1@s uk-child-width-1-3@m" >
					        	<?php  
					        		while ($portafolioMovil3 = $consultamovil3 -> fetch_assoc()) :
					        		$fondo = $urlPortafolios.$portafolioMovil3['id'].'.png';
					        	?> 
					        	<?php  if($portafolioMovil3['url'] != ""):?>
						        <a class="uk-grid-collapse" href="<?= $portafolioMovil3['url'] ?>" target="_blank">
						        <?php endif ?>
					            <li class="uk-flex uk-flex-center">
					                <div class="uk-background-contain uk-background-muted uk-height-medium uk-panel" 
		                              style="height: calc(100vh / 2);width:calc(100vh / 2); background: url('<?= $fondo ?>');background-position: bottom;background-repeat:no-repeat;background-size:100% 100%;">
		                            </div>
					            </li>
					            <?php  if($portafolioMovil3['url'] != ""):?>
					        	</a>
					            <?php endif ?>
								<?php endwhile  ?>
					        </ul>
					    </div>
					    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
					</div>
			    </div>
	    	</div>
	    </div>
		<div class="uk-width-auto@m uk-grid-collapse teleft-0 height-100 uk-visible@m"  
	    style="margin-top:0!important;">
	        <div class="uk-grid-collapse uk-align-center"  style="margin-top:0!important;">
	            <div class="texto-vertical-2" style="margin-top:30px;margin-right:30px;margin-left:0;color:#bf9d59"> PORTAFOLIO &nbsp;</div>
	        </div>
	    </div>
	    <div class="uk-width-auto@m uk-grid-collapse teleft-0 height-100 uk-hidden@m"  
	    style="margin-top:0!important;">
	        <div class="uk-grid-collapse uk-align-center"  style="margin-top:0!important;">
	            <div class="uk-text-center" style="margin-top:30px;margin-right:30px;margin-left:0;color:#bf9d59">  &nbsp;</div>
	        </div>
	    </div>
	</div>
	<?=$footer?>
</section>



<?=$scriptGNRL?>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
	$('.slick-carousel1, .slick-carousel2, .slick-carousel3').slick({
	  autoplay:true,
	  infinite: true,
	  vertical:true,
	  verticalSwiping:true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  prevArrow: $('.top-arrow'),
	  nextArrow: $('.bottom-arrow')
	});
</script>

</body>
</html>