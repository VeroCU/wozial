<!DOCTYPE html>
<?=$headGNRL?>
<body>

<?php
 	// CONSULTAMOS LOS PORTAFOLIO(proyectos) 
 	$urlPortafolios="./img/contenido/portafolio/";
 	$consultaPortafolios = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1");
 	$consultamovil1 = $CONEXION -> query("SELECT * FROM portafolio WHERE estatus= 1");
?>

<section class="full-container uk-grid-collapse">
	<?=$header?>
	<div class="uk-grid-collapse left-0" uk-grid>
		<div class="uk-hidden@m margin-v-70"> &nbsp; </div>
	    
	    <?= $headerizq?>
	    
	    <div class="uk-width-expand@m uk-grid-collapse teleft-0 height-100" id="iniciemos">
	        <div class="uk-flex uk-flex-middle height-100">
	        	<div class="uk-width-1-1">
	        		<div class="uk-container uk-container-small pad-container container_empezar">
		        		<div class="margin-top-20">
		        			<h2 class="uk-text-left uk-grid-collapse h2-buttom" style="">
			        			<?= $about_titulo ?> <br><span class="text-dorado"><?= $about_titulo_dorado ?></span>
			        		</h2>
			        	</div>
			        	<div class="margin-top-10">
			        		<img class="uk-align-left bordes-0  width-56" src="./img/design/visual.png">
			        		<h4 class="uk-text-left uk-grid-collapse uk-flex-middle"><?= $about_subtitulo1 ?></h4>
		        			<div class="uk-text-left uk-grid-collapse cero-bordes p empezamos" style="margin:0!important;padding:0!important;">
		        				<?= $about1 ?>
		        			</div>
		        		</div>	
		        		<div class="">
		        			<img class="uk-align-left bordes-0  width-56" src="./img/design/neuro.png">
			        		<h4 class="uk-text-left uk-grid-collapse uk-flex-middle"><?= $about_subtitulo2 ?></h4>
		        			<div class="uk-text-left uk-grid-collapse cero-bordes p empezamos" style="margin:0!important;padding:0!important;">
		        				<?= $about2 ?>
		        			</div>
		        		</div>
			        	<div class="">
		        			<img class="uk-align-left bordes-0  width-56" src="./img/design/tecnologia.png">
			        		<h4 class="uk-text-left uk-grid-collapse uk-flex-middle"><?= $about_subtitulo3 ?></h4>
		        			<div class="uk-text-left uk-grid-collapse cero-bordes p empezamos" style="margin:0!important;padding:0!important;">
		        				<?= $about3 ?>
		        			</div>
		        		</div>

	        			<div class="uk-flex uk-flex-center uk-grid-collaps left-0" uk-grid>
						    <div class="uk-width-1-2@m uk-grid-collapse">
						        <a class="uk-button uk-button-default k-align-center uk-button-primary margin-left-10" href="#modal-center" uk-toggle>
						    		<p style="margin-top:14px">COTIZACIÓN AHORA</p>
						    	</a>
						    </div>
						</div>
	        		</div>
	        		
				</div>
	        </div>
	    </div>
	    

	    <?php echo $sliderderecha ?>

	    <?php echo $slidermovil ?>
	    
	    <?php echo $btnportafolio ?>
	</div>
	<?=$footer?>
</section>



<?=$scriptGNRL?>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
	$('.slick-carousel').slick({
	  autoplay:true,
	  infinite: true,
	  vertical:true,
	  verticalSwiping:true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  prevArrow: $('.top-arrow'),
	  nextArrow: $('.bottom-arrow')
	});
</script>

</body>
</html>